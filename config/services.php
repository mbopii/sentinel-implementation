<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '2324043380946056',
        'client_secret' => 'b9e1a9a147b6c9b4b24b5963cd7ceba8',
        'redirect' => 'https://massari.com.py/login/facebook/callback',
    ],
    'twilio' => [
        'accountSid' => 'AC810c9d40496050235b3fafc42366b444',
        'apiKey' => 'SKfb037c1334360ab481f59a47cd29597f',
        'apiSecret' => 'W0mD4VyMLz9Neidh8YLkiGeWfkI9CJbg',
        'ipmServiceSid' => ''
    ],

];
