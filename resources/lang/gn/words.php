<?php

return [
    'header' => 'Ñepyrũ',
    'store' => 'Ñemuha',
    'header_cart' => 'Ygape',
    'header_checkout' => 'Mboepy',
    'header_register' => 'Téra Ryru',
    'add_to_cart' => 'Emoi alaka-pe',
    'go_to_checkout' => "Hepyme'e",
    'keep_buying' => 'Jajoguamina',
    'go_to_store' => 'Ñemuha-pe',
    'buy_now' => "Ejogua ko'anga",
    'header_hi' => "Mba'eteko",
    'search_bar' => "Ejeka ko'ape mba'epa reipotá ...",
    'contact_title' => 'Ehenói-py'

];