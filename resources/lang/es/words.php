<?php

return [
    'header' => 'Inicio',
    'store' => 'Tienda',
    'header_cart' => 'Carrito',
    'header_checkout' => 'Caja',
    'header_register' => 'Entrar o Registarte',
    'add_to_cart' => 'Agregar al carrito',
    'go_to_checkout' => "Finalizar Compra",
    'keep_buying' => 'Seguir Comprando',
    'go_to_store' => 'Ir a la tienda',
    'buy_now' => 'Comprar Ahora',
    'header_hi' => 'Hola',
    'search_bar' => 'Busque aquí su producto .. !',
    'contact_title' => 'Contactanos'

];