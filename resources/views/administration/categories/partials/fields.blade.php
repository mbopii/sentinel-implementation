<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('description' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Categoria</label>
    </div>
</div>
<div class="form-group">
    <h2 class="card-inside-title">Imagen de Referencia</h2>
    <div class="form-line">
        {!! Form::file('category_image' , ['class' => 'form-control', 'id' => 'product_image']) !!}
    </div>
</div>
<br>
<button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>