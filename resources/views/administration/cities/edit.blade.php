@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Editar Ciudad
                        <small>{{ $department->description }} -> {{ $city->description }}</small>
                    </h2>
                </div>
                <div class="body">
                    {!! Form::model($city, ['route' => ['cities.update', $department->id, $city->id], 'method' => 'patch', 'files' => 'true']) !!}
                    @include('administration.cities.partials.fields')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->
@endsection

