@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Ciudades
                        <small>Seleccione una ciudad para continuar o agregue una nueva en el menu</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="{{ route('cities.create', ['department_id'=> $department->id]) }}">Nuevo</a>
                                </li>
                                <li><a href="{{ route('departments.index') }}">Ver todos los Departamentos</a></li>
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        @if($cities->isEmpty())
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            No existen ciudades asignadas para el departamento: {{ $department->description }}
                            <small>Agregue una nueva en el menu</small>
                        </h2>
                    </div>
                </div>
            </div>
        @else

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Descripción</th>
                                <th>Creado</th>
                                <th>Modificado</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cities as $city)
                                <tr class="even pointer" data-id="{{ $city->id }}">
                                    <th scope="row">{{ $city->id }}</th>
                                    <td>{{ $city->description }}</td>
                                    <td>{{ $city->created_at }}</td>
                                    <td>{{ $city->updated_at }}</td>
                                    <td>
                                        <div class="icon-button-demo">
                                            <a href="{{ route('cities.edit', ['id' => $city->id, 'department_id'=> $department->id]) }}">
                                                <button class="btn btn-info waves-effect" type="button">
                                                    <i class="material-icons">border_color</i>
                                                </button>
                                            </a>
                                            <button class="btn btn-danger waves-effect btn-delete" type="button">
                                                <i class="material-icons">delete_forever</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $cities->links() }}
                    </div>
                </div>
            </div>
        @endif
    </div>

    {!! Form::open(['route' => ['categories.destroy', 'id' => ':ROW_ID', 'department_id' => $department->id], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('css')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>
@endsection
@section('js')
    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>
    @include('administration.templates.partials.delete_row')
@endsection