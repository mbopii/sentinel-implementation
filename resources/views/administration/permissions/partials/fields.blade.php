<div class="form-group row">
    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Descripción</label>
    <div class="col-sm-9">
        {{ Form::text('description', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>
<div class="form-group row">
    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Permiso</label>
    <div class="col-sm-9">
        {{ Form::text('permission', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}

    </div>
</div>
