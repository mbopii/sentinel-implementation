@extends('administration.templates.layout')
@section("breadcrumbs")
    <h4 class="page-title">Permisos</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Lista</li>
            </ol>
        </nav>
    </div>
@endsection
@section('body')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Permisos Cargados</h5>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Permiso</th>
                        <th scope="col">Creado</th>
                        <th scope="col">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($permissions as $permission)
                    <tr data-id="{{ $permission->id }}">
                        <th scope="row">{{ $permission->id }}</th>
                        <td>{{ $permission->description }}</td>
                        <td>{{ $permission->permission }}</td>
                        <td>{{ $permission->created_at->format("Y-m-d") }}</td>
                        <td>
                            <a href="{{ route('permissions.edit', $permission->id) }}">
                                <button class="btn btn-primary btn-sm">
                                    <i class="fa fa-pencil-alt"></i> Editar
                                </button>
                            </a>

                            <button class="btn btn-danger btn-sm btn-delete" type="button">
                                <i class="fa fa-times"></i> Eliminar
                            </button>
                        </td>
                    </tr>
                   @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => ['permissions.destroy',':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('admin_css')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/assets/extra-libs/sweetalert/sweetalert2.css' }}" rel="stylesheet" />
@endsection

@section('admin_js')
    <script src="{{ '/admin/assets/extra-libs/sweetalert/sweetalert2.min.js' }}"></script>
    @include('administration.templates.partials.delete_row')
@endsection