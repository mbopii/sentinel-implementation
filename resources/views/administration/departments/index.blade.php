@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Departamentos
                        <small>Seleccione un Departamento para continuar o agregue una nueva en el menu</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{ route('departments.create') }}">Nuevo</a></li>
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        @if($departments->isEmpty())
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            No existen departamentos Cargados
                            <small>Agregue una nueva en el menu</small>
                        </h2>
                    </div>
                </div>
            </div>
        @else
            @foreach($departments as $department)
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 dlt_card">
                    <a href="{{ route('cities.index', ['department_id' => $department->id]) }}">
                        <div class="card">
                            <div class="card-image">
                                <img src="{{ "/admin/images/departments/{$department->id}.jpg"  }}" width="100%"
                                     height="100%"
                                     alt="">
                            </div>
                            <div class="header">
                                <h2>
                                    {{ $department->description }}
                                    <small></small>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                           role="button"
                                           aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right" data-id="{{ $department->id }}">
                                            <li>
                                                <a href="{{ route('departments.edit', $department->id) }}">Editar</a>
                                            </li>
                                            <li><a href="#" class="btn-delete">Eliminar</a></li>
                                            {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </a>
                </div>

            @endforeach
        @endif
    </div>

    {!! Form::open(['route' => ['departments.destroy', ':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('css')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>
@endsection
@section('js')
    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>
    @include('administration.templates.partials.delete_card')
@endsection