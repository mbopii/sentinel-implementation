@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Nuevo Producto
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    {!! Form::open(['route' => ['products.store'], 'method' => 'post', 'files' => 'true']) !!}
                    <div class="row">
                        <div class="col-md-7">
                            @include('administration.products.partials.fields')
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->
@endsection

@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>

@endsection

@section('js')
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>

    <script>
        var _URL = window.URL;
        $("#product_image").change(function (e) {
            var file, img;
            if ((file = this.files[0])) {
                img = new Image();
                img.onload = function () {
                    if (this.width <= 640 && this.height <= 853) {
                        $("#submitButton").removeAttr('disabled');
                    } else {
                        alert("Tu imagen no cumple con el tamaño especificado, debe de ser de al menos 853px por 640px");
                        $("#submitButton").attr('disabled', 'disabled');
                    }
                };
                img.src = _URL.createObjectURL(file);
            }
        })
    </script>
    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>

    <script>
        $('#add_complex').click(function () {
            var product_id = $('#select_product').val();
            var product_name = $("#select_product option:selected").text();
            var quantity = $('#select_quantity').val();
            console.log(product_id);
            console.log(product_name);
            $("#complex_table tbody").append('' +
                '<tr id="' + product_id + '">' +
                '<td> ' + product_id + ' <input type="hidden" name=complex_product_id[] value="' + product_id + '"></td>' +
                '<td> ' + product_name + ' </td>' +
                '<td> ' + quantity + ' <input type="hidden" name=complex_quantity[] value="' + quantity + '"></td>' +
                '<td>' +
                '<button class="btn btn-danger waves-effect btn-delete-complex" type="button">' +
                '<i class="material-icons">delete_forever</i></button>' +
                '</td>' +
                '</tr> ');

            $('#complex_table tbody tr td').on('click', 'button.btn-delete-complex', function (e) {
                console.log("Delete complex product - Binding new event");
                e.preventDefault();
                var row = $(this).parents('tr');
                var id = row[0].id;
                console.log(id);
                swal({
                        title: "Atención!",
                        text: "Está a punto de borrar el registro, está seguro?.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, eliminar!",
                        cancelButtonText: "No, cancelar!",
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true,
                        closeOnCancel: true
                    },

                    function (isConfirm) {
                        if (isConfirm) {
                            var form = $('#form-delete-complex');
                            console.log(id);
                            var url = form.attr('action').replace(':ROW_ID', id);
                            var data = form.serialize();
                            var type = "";
                            var title = "";
                            $.post(url, data, function (result) {

                                console.log(result.message);
                                if (result.error == false) {
                                    row.fadeOut();
                                    type = "success";
                                    title = "Operación realizada!";
                                } else {
                                    type = "error";
                                    title = "No se pudo realizar la operación"
                                }
                                swal({title: title, text: result.message, type: type, confirmButtonText: "Aceptar"});
                            }).fail(function () {
                                swal('No se pudo realizar la petición.');
                            });

                        }
                    });
            });
            $('#productsModal').modal('toggle');
        });

    </script>
@endsection