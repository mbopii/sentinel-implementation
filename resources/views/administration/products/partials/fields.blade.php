<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('name' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Nombre</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('description' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Descripcion</label>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-4">
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('quantity' , null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                <label class="form-label">Stock Disponible</label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('price' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
                <label class="form-label">Precio</label>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-4">
        <div class="form-line">
            <label id="brand_id">Marca</label>
            {!! Form::select('brand_id' , $brands , ['class' => 'form-control show-tick', 'id' => 'description']) !!}
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <label id="category_id">Categoria</label>
            <select name="category_id" id="">
                <optgroup label="Sin SubCategoria">
                    @foreach ($categories as $c)
                        @if (!isset($c->subcategories[0]->id))
                            <option value="cat_{{ $c->id }}">{{ $c->description }}</option>
                        @endif
                    @endforeach
                </optgroup>
                @foreach ($categories as $c)
                    @if (isset($c->subcategories))
                        <optgroup label="Categoria {{ $c->description }}">
                            @foreach ($c->subcategories as $subcategory)
                                <option value="{{ $subcategory->id }}">{{ $subcategory->description }}</option>
                            @endforeach
                        </optgroup>
                    @endif
                @endforeach
            </select>

        </div>
    </div>
    <div class="col-md-2">
        <div class="demo-checkbox">
            <div class="demo-checkbox">
                <input type="checkbox" id="featured" name="featured"
                       @if(isset($product) &&  $product->featured) checked @endif/>
                <label for="featured">Destacado</label>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-4">
        <div class="demo-checkbox">
            <div class="demo-checkbox">
                <input type="checkbox" id="discount" name="discount"
                       @if(isset($product) && $product->discount) checked @endif/>
                <label for="discount">Descuento</label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('discount_percentage' , null , ['class' => 'form-control', 'id' => 'discount_percentage']) !!}
                <label class="form-label">% de Descuento</label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('discount_quantity' , null , ['class' => 'form-control', 'id' => 'discount_quantity']) !!}
                <label class="form-label">Cantidad de Items</label>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-offset-4 col-md-4">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('discount_percentage2' , null , ['class' => 'form-control', 'id' => 'discount_percentage2']) !!}
                <label class="form-label">% de Descuento</label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('discount_quantity2' , null , ['class' => 'form-control', 'id' => 'discount_quantity2']) !!}
                <label class="form-label">Cantidad de Items</label>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-4">
        <div class="demo-checkbox">
            <div class="demo-checkbox">
                <input type="checkbox" id="complex" name="complex"
                       @if(isset($product) && $product->complex_product) checked @endif/>
                <label for="complex">Producto Compuesto</label>
            </div>
        </div>
    </div>
    <div class="col-md-8">

        <table id="complex_table" class="table table-bordered">
            <thead>
            <tr>
                <td>Id</td>
                <td>Nombre</td>
                <td>Cantidad</td>
                <td>Acciones</td>
            </tr>
            </thead>
            <tbody>
            @isset($product)
                @foreach($product->complexItems as $item)
                    <tr id="{{ $item->product->id }}">
                        <td>{{ $item->product->id }}<input type="hidden" name=complex_product_id[] value="{{ $item->product->id }}"></td>
                        <td>{{ $item->product->name }}</td>
                        <td>{{ $item->quantity }}<input type="hidden" name=complex_quantity[] value="{{ $item->quantity }}"></td>
                        <td>
                            <button class="btn btn-danger waves-effect btn-delete-complex" type="button">
                                <i class="material-icons">delete_forever</i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

        <div class="form-group form-float">
            <button type="button" data-toggle="modal" data-target="#productsModal"
                    class="btn btn-primary m-t-15 waves-effect">Agregar Producto
            </button>
        </div>
        <br>
        <div id="complex_products">

        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('weight' , null , ['class' => 'form-control', 'id' => 'discount_percentage']) !!}
                <label class="form-label">Peso</label>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('height' , null , ['class' => 'form-control', 'id' => 'discount_percentage']) !!}
                <label class="form-label">Altura</label>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('width' , null , ['class' => 'form-control', 'id' => 'discount_quantity']) !!}
                <label class="form-label">Ancho</label>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group form-float">
            <div class="form-line">
                {!! Form::text('length' , null , ['class' => 'form-control', 'id' => 'discount_quantity']) !!}
                <label class="form-label">Largo</label>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <h2 class="card-inside-title">Imagen de Referencia</h2>
    <div class="form-line">
        {!! Form::file('product_image[]' , ['class' => 'form-control', 'id' => 'product_image', 'multiple' => 'multiple']) !!}
    </div>
</div>

<br>
<button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>

<div class="modal fade" id="productsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Agregar Producto</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-float">
                    <div class="form-line">
                        <p>
                            <b>Elija un producto</b>
                        </p>
                        <div class="row">
                            <select id="select_product" name="select_product" class="form-control show-tick"
                                    data-live-search="true">
                                <option value="0">Seleccione</option>
                                @foreach($productsJson as $product)
                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-line">
                                <div class="form-group">
                                    {!! Form::text('select_quantity' , null , ['class' => 'form-control', 'id' => 'select_quantity']) !!}
                                    <label class="form-label">Cantidad</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="add_complex" class="btn btn-link waves-effect">Agregar</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>
