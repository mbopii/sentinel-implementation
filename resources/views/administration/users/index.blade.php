@extends('administration.templates.layout')
@section("breadcrumbs")
    <h4 class="page-title">Permisos</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Lista</li>
            </ol>
        </nav>
    </div>
@endsection
@section('body')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Usuarios Cargados</h5>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Cédula</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Creado</th>
                        <th scope="col">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr data-id="{{ $user->id }}">
                        <th scope="row">{{ $user->id }}</th>
                        <td>{{ $user->description }}</td>
                        <td>{{ $user->idnum }}</td>
                        <td>{{ $user->telephone }}</td>
                        <td>{{ $user->created_at->format("Y-m-d") }}</td>
                        <td>
                            <a href="{{ route('users.edit', $user->id) }}">
                                <button class="btn btn-primary btn-sm">
                                    <i class="fa fa-pencil-alt"></i> Editar
                                </button>
                            </a>

                            <button class="btn btn-danger btn-sm btn-delete" type="button">
                                <i class="fa fa-times"></i> Eliminar
                            </button>
                        </td>
                    </tr>
                   @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => ['users.destroy',':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('admin_css')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/assets/extra-libs/sweetalert/sweetalert2.css' }}" rel="stylesheet" />
@endsection

@section('admin_js')
    <script src="{{ '/admin/assets/extra-libs/sweetalert/sweetalert2.min.js' }}"></script>
    @include('administration.templates.partials.delete_row')
@endsection