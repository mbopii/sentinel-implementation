<div class="form-group row">
    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Descripción</label>
    <div class="col-sm-9">
        {{ Form::text('description', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>
<div class="form-group row">
    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Nombre de Usuario</label>
    <div class="col-sm-9">
        {{ Form::text('username', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>

<div class="form-group row">
    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email</label>
    <div class="col-sm-9">
        {{ Form::text('email', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>
<div class="form-group row">
    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Numero de Cédula</label>
    <div class="col-sm-9">
        {{ Form::text('idnum', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>
<div class="form-group row">
    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Fecha de Nacimiento</label>
    <div class="col-sm-9">
        {{ Form::text('birthday', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>
<div class="form-group row">
    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Numero de Teléfono</label>
    <div class="col-sm-9">
        {{ Form::text('telephone', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>

<div class="form-group row">
    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Dirección</label>
    <div class="col-sm-9">
        {{ Form::text('address', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>


<div class="form-group row">
    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contrasena</label>
    <div class="col-sm-9">
        {{ Form::password('password', ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>
<div class="form-group row">
    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Confirmar Contrasena</label>
    <div class="col-sm-9">
        {{ Form::password('password_confirmation', ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>

<div class="form-group row">
    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Rol de Usuario</label>
    <div class="col-sm-9">
        {{ Form::select('role_id', $rolesList, null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>
</div>
