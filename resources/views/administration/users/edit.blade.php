@extends('administration.templates.layout')
@section("breadcrumbs")
    <h4 class="page-title">Usuarios</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Lista</li>
            </ol>
        </nav>
    </div>
@endsection
@section('body')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}
                <div class="card-body">
                    <h4 class="card-title">Nuevo Permiso</h4>
                    @include("administration.users.partials.fields")
                    <div class="border-top">
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
@endsection