@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    {!! Form::open(['route' => ['sales.store'], 'method' => 'post', 'files' => 'true']) !!}

    <div class="row clearfix">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Venta
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('administration.sales.partials.fields')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Facturación
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('administration.sales.partials.extra_data')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Detalles
                        <small>
                            <div class="form-group form-float">
                                <button type="button" data-toggle="modal" data-target="#productsModal"
                                        class="btn btn-primary m-t-15 waves-effect">Agregar Producto
                                </button>
                            </div>
                        </small>
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('administration.sales.partials.fields_table')
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    {{--Modal para Nuevo Cliente--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="new-client-modal" aria-labelledby="editor-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content" role="document">
                <form id="new_client" class="modal-content form-horizontal" action="{{ route('users.admin.store') }}"
                      method="post">
                    <div class="modal-header">
                        <h4 class="modal-title" id="editor-title">Agregar Nuevo Cliente</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-line">
                                <label>Nombre</label>
                                {!! Form::text('description' , null , ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                                <label>Cedula</label>
                                {!! Form::text('idnum' , null , ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                                <label>Razon Social</label>
                                {!! Form::text('tax_name' , null , ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                                <label>Ruc</label>
                                {!! Form::text('tax_code' , null , ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                                <label>Dirección</label>
                                {!! Form::text('address' , null , ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-line">
                                <label>Nro de Casa</label>
                                {!! Form::text('number' , null , ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label>Teléfono</label>
                                {!! Form::text('telephone' , null , ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="role_id">Ciudad</label>
                            {!! Form::select('city_id' , $cities , ['class' => 'form-control show-tick', 'id' => 'description']) !!}
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->
@endsection

@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>

@endsection

@section('js')
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>

    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>
    <script>
        let rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
        var xhr;
        $('#search_client').click(function (e) {
            e.preventDefault();
            $('#client_name').val('');
            $('#client_code').val('');
            $('#client_tax_code').val('');
            $('#client_tax_name').val('');
            $('#client_id_num').val('');
            $('#client_address').val('');
            $('#client_telephone').val('');
            $('#client_city_id').val('');
            e.preventDefault();
            console.log('Search new Client');
            var idnum = $('#client_idnum').val();
            xhr && xhr.abort();
            xhr = $.ajax({
                url: rootURL + 'administration/users/search/' + idnum,
                method: "GET",
                success: function (result) {
                    console.log(result);
                    var response = JSON.parse(result);
                    if (response.error == false) {
                        console.log("Client found");
                        $('#user_description').val(response.client.description);
                        $('#user_idnum').val(response.client.idnum);
                        $('#user_email').val(response.client.email);
                        $('#user_birthday').val(response.client.birthday);
                        var option = '';
                        var j = 1;
                        for (var i = 0; i < response.addresses.length; i++) {
                            $(".addresses")
                                .append('<input name="address_id" type="radio" id="radio_' + j + '" value="' + response.addresses[i].id + '"/>' +
                                    '<label for="radio_' + j + '">' + response.addresses[i].address + '</label><br>');
                            j++;
                        }
                        var a = 1;
                        for (var k = 0; k < response.invoices.length; k++) {
                            $(".invoices")
                                .append('<input name="invoice_id" type="radio" id="radio_t' + a + '" value="' + response.invoices[k].id + '"/>' +
                                    '<label for="radio_t' + a + '">' + response.invoices[k].tax_name + ' / ' + response.invoices[k].tax_code + '</label><br>');
                            a++;
                        }
                    } else {
                        if (response.code == 10) {
                            $("#new-client-modal").modal()
                        }
                    }
                },
                error: function () {
                    alert("this is not working");
                }
            });
        })
    </script>
    <script>
        $('#add_complex').click(function () {
            var product_id = $('#select_product').val();
            var product_name = $("#select_product option:selected").text();
            var quantity = $('#select_quantity').val();
            xhr && xhr.abort();
            xhr = $.ajax({
                url: rootURL + 'administration/products/single/' + product_id,
                method: "GET",
                success: function (result) {
                    var response = JSON.parse(result);
                    if (response.error == false) {
                        var price = response.product.price;
                        var subtotal = 0;
                        var discount = 0;
                        var flag = false;
                        if (response.product.discount && parseInt(quantity) >= parseInt(response.product.discount_quantity)) {
                            flag = true;
                            discount = (price * quantity) * (response.product.discount_percentage / 100);
                            subtotal = price * quantity - discount;
                        } else {
                            subtotal = price * quantity
                        }
                        console.log(response.product);
                        $("#complex_table tbody").append('' +
                            '<tr id="' + product_id + '">' +
                            '<td> ' + product_id + ' <input type="hidden" name="product_id[]" value="' + product_id + '"></td>' +
                            '<td> ' + product_name + ' </td>' +
                            '<td> ' + quantity + ' <input type="hidden" name="quantity[]" value="' + quantity + '"></td>' +
                            '<td> ' + price + '</td>' +
                            '<td> ' + discount + '<input type="hidden" name="discount[]" value="' + discount + '"></td>' +
                            '<td> ' + subtotal + ' </td>' +
                            '<td>' +
                            '<button class="btn btn-danger waves-effect btn-delete-complex" type="button">' +
                            '<i class="material-icons">delete_forever</i></button>' +
                            '</td>' +
                            '</tr> ');
                    }

                },
                error: function () {
                    alert("this is not working");
                }
            });

            $('#select_product').val('');
            $('#select_quantity').val('');

            $('#complex_table tbody tr td').on('click', 'button.btn-delete-complex', function (e) {
                console.log("Delete complex product - Binding new event");
                e.preventDefault();
                var row = $(this).parents('tr');
                var id = row[0].id;
                console.log(id);
                swal({
                        title: "Atención!",
                        text: "Está a punto de borrar el registro, está seguro?.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, eliminar!",
                        cancelButtonText: "No, cancelar!",
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true,
                        closeOnCancel: true
                    },

                    function (isConfirm) {
                        if (isConfirm) {
                            row.fadeOut();
                            var type = "success";
                            var title = "Operación realizada!";

                            swal({title: title, text: result.message, type: type, confirmButtonText: "Aceptar"});
                        }
                    });
            })
            ;
            $('#productsModal').modal('toggle');
        })
        ;

    </script>
@endsection