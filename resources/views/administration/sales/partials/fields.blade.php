<div class="row clearfix">
    <div class="col-md-10">
        <div class="form-group">
            <div class="form-line">
                <label for="cc-payment" class="control-label mb-1">Cliente</label>
                {!! Form::text('client_idnum', null, ['class' => 'form-control', 'id' => 'client_idnum']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <button class="btn btn-primary m-t-15 waves-effect" id="search_client">Buscar</button>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-3">
        <div class="form-line">
            <label>Nombre</label>
            {!! Form::text('description' , null , ['class' => 'form-control', 'id' => 'user_description', 'disabled' => 'disabled']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-line">
            <label>Cédula</label>
            {!! Form::text('idnum' , null , ['class' => 'form-control', 'id' => 'user_idnum', 'disabled' => 'disabled']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-line">
            <label>Email</label>
            {!! Form::text('email' , null , ['class' => 'form-control', 'id' => 'user_email', 'disabled' => 'disabled']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-line">
            <label>Fecha de Nacimiento</label>
            {!! Form::text('birthday' , null , ['class' => 'form-control', 'id' => 'user_birthday', 'disabled' => 'disabled']) !!}
        </div>
    </div>
</div>
