@extends('administration.templates.layout')
@section('content')
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
    {!! Form::open(['route' => ['purchases.store'], 'method' => 'post', 'files' => 'true']) !!}

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Nueva Compra
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('administration.purchases.partials.fields')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Detalles
                        {{--<small>With floating label</small>--}}
                    </h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('administration.purchases.partials.fields_table')
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    <!-- Vertical Layout | With Floating Label -->
@endsection

@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>

@endsection

@section('js')
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>

    <script>
        var _URL = window.URL;
        $("#product_image").change(function (e) {
            var file, img;
            if ((file = this.files[0])) {
                img = new Image();
                img.onload = function () {
                    if (this.width <= 640 && this.height <= 853) {
                        $("#submitButton").removeAttr('disabled');
                    } else {
                        alert("Tu imagen no cumple con el tamaño especificado");
                        $("#submitButton").attr('disabled', 'disabled');
                    }
                };
                img.src = _URL.createObjectURL(file);
            }
        })
    </script>
    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>

    <script>
        $('#add_complex').click(function () {
            var product_id = $('#select_product').val();
            var product_name = $("#select_product option:selected").text();
            var quantity = $('#select_quantity').val();
            var purchase_price = $('#purchase_price').val();
            console.log(product_id);
            console.log(product_name);
            $("#complex_table tbody").append('' +
                '<tr id="' + product_id + '">' +
                '<td> ' + product_id + ' <input type="hidden" name=purchase_product_id[] value="' + product_id + '"></td>' +
                '<td> ' + product_name + ' </td>' +
                '<td> ' + quantity + ' <input type="hidden" name=purchase_quantity[] value="' + quantity + '"></td>' +
                '<td> ' + purchase_price + ' <input type="hidden" name=purchase_unit_price[] value="' + purchase_price + '"></td>' +
                '<td>' +
                '<button class="btn btn-danger waves-effect btn-delete-complex" type="button">' +
                '<i class="material-icons">delete_forever</i></button>' +
                '</td>' +
                '</tr> ');

            $('#select_product').val('');
            $('#select_quantity').val('');
            $('#purchase_price').val('');

            $('#complex_table tbody tr td').on('click', 'button.btn-delete-complex', function (e) {
                console.log("Delete complex product - Binding new event");
                e.preventDefault();
                var row = $(this).parents('tr');
                var id = row[0].id;
                console.log(id);
                swal({
                        title: "Atención!",
                        text: "Está a punto de borrar el registro, está seguro?.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, eliminar!",
                        cancelButtonText: "No, cancelar!",
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true,
                        closeOnCancel: true
                    },

                    function (isConfirm) {
                        if (isConfirm) {
                            row.remove();
                            var type = "success";
                            var title = "Operación realizada!";

                            swal({title: title, text:'Eliminado correctamente', type: type, confirmButtonText: "Aceptar"});
                        }
                    });
            })
            ;
            $('#productsModal').modal('toggle');
        })
        ;

    </script>
@endsection