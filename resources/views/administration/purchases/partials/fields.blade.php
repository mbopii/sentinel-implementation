
<div class="row clearfix">
    <div class="col-md-4">
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('purchase_date', null, ['class' => 'datepicker form-control', 'id' => 'description']) !!}
                <label class="form-label">Fecha de Compra</label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('invoice_number' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
                <label class="form-label">Numero de Factura</label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('stamping' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
                <label class="form-label">Timbrado</label>
            </div>
        </div>
    </div>
</div>
