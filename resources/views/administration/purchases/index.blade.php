@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Compras
                        <small>Filtros de Busqueda</small>
                    </h2>
                </div>
                <div class="body">
                    <form action="{{ route('purchases.index') }}">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('date_start', null, ['class' => 'datepicker form-control', 'id' => 'description']) !!}
                                        <label class="form-label">Desde</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('date_end', null, ['class' => 'datepicker form-control', 'id' => 'description']) !!}
                                        <label class="form-label">Hasta</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('invoice_number', null, ['class' => 'form-control', 'id' => 'description']) !!}
                                        <label class="form-label">Numero de Factura</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <input type="hidden" name="q" value="q">
                                <button type="submit" class="btn btn-info">Filtrar</button>
                                {{--<button class="btn btn-success" name="download" value="xls">Excel</button>--}}
                                <a href="{{ route('purchases.index') }}" class="btn btn-warning">Ver Todos</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Compras
                        <small>Cargadas en el sistema</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{ route('purchases.create') }}">Nuevo</a></li>
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Fecha de Compra</th>
                            <th>Factura</th>
                            <th>Monto Total</th>
                            <th>Factura anulada</th>
                            <th>Creado</th>
                            <th>Modificado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($purchasesHeader as $p)
                            <tr class="even pointer" data-id="{{ $p->id }}">
                                <th scope="row">{{ $p->id }}</th>
                                <td>{{ $p->purchase_date }}</td>
                                <td>{{ $p->invoice_number }}</td>
                                <td>{{ number_format($p->total_amount, 0, ',', '.') }}</td>
                                <td>@if($p->canceled) Si @else No @endif</td>
                                <td>{{ $p->created_at->format('Y-m-d') }}</td>
                                <td>{{ $p->updated_at->format('Y-m-d') }}</td>
                                <td>
                                    <div class="icon-button-demo">
                                        <a href="{{ route('purchases.show', $p->id) }}">
                                            <button class="btn btn-info waves-effect" type="button">
                                                <i class="material-icons">zoom_in</i>
                                            </button>
                                        </a>

                                    </div>
                                    @if(!$p->canceled)
                                        {!! Form::open(['route' => ['purchases.destroy',$p->id], 'method' => 'DELETE',
                                     'id' => 'form-delete']) !!}
                                        <button class="btn btn-danger waves-effect btn-delete" type="submit">
                                            <i class="material-icons">delete_forever</i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $purchasesHeader->appends(['date_start' => $date_start, 'q' => $q, 'date_end' => $date_end, 'invoice_number' => $invoice_number])->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
@endsection


@section('css')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ '/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' }}"
          rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="{{ '/admin/plugins/waitme/waitMe.css' }}" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="{{ '/admin/plugins/bootstrap-select/css/bootstrap-select.css' }}" rel="stylesheet"/>
@endsection

@section('js')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>
    <!-- Moment Plugin Js -->
    <script src="{{ '/admin/plugins/momentjs/moment.js' }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ '/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js' }}"></script>

    <script src="{{ '/admin/js/pages/forms/basic-form-elements.js' }}"></script>

    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>
@endsection
