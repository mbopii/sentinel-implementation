<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('description' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Categoria</label>
    </div>
</div>

<div class="form-line">
    <label id="category_id">Categoria</label>
    {!! Form::select('category_id' , $categories , ['class' => 'form-control show-tick', 'id' => 'description']) !!}
</div>
<br>
<button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>