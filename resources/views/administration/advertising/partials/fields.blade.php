<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('tittle' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Titulo</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('description' , null , ['class' => 'form-control', 'id' => 'description']) !!}
        <label class="form-label">Descripción</label>
    </div>
</div>

<div class="form-group form-float">
    <div class="form-line">
        {!! Form::text('url' , null , ['class' => 'form-control', 'id' => 'permission']) !!}
        <label class="form-label">Enlace</label>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-6">
        <div class="form-line">
            <label id="section_id">Seccion</label>
            {!! Form::select('section_id' , $sections , ['class' => 'form-control show-tick', 'id' => 'selectSection']) !!}
        </div>
    </div>
    <div class="col-md-5">
        <div class="demo-checkbox">
            <input type="checkbox" id="active" name="active"/>
            <label for="featured">Activo</label>
        </div>
    </div>
</div>

<div class="form-group">
    <h2 class="card-inside-title">Imagen de Referencia</h2>
    <div class="form-line">
        {!! Form::file('banner_image' , ['class' => 'form-control', 'id' => 'banner_image']) !!}
    </div>
</div>

{{--<input type="checkbox" id="remember_me_2" class="filled-in">--}}
{{--<label for="remember_me_2">Remember Me</label>--}}
<br>
<button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>