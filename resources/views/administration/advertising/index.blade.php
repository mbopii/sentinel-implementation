@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Publicidades
                        <small>Dentro de la tienda</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{ route('advertising.create') }}">Nuevo</a></li>
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body table-responsive">
                    @foreach($advertising as $a)
                    <div class="row" id="row_{{ $a->id }}">
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th style="width:50%">Titulo:</th>
                                        <td>{{ $a->tittle }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">Descripcion:</th>
                                        <td>{{ $a->description }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">Seccion:</th>
                                        <td>{{ $a->section->description }}</td>
                                    </tr>
                                    <tr>
                                        <th>Link</th>
                                        <td>{{ $a->url }}</td>
                                    </tr>
                                    <tr>
                                        <th>Creado</th>
                                        <td>{{ $a->created_at->format('Y-m-d') }}</td>
                                    </tr>
                                    <tr data-id="{{ $a->id }}">
                                        <th>Acciones</th>
                                        <td>
                                            <div class="icon-button-demo">
                                                <a href="{{ route('advertising.edit', $a->id) }}">
                                                    <button class="btn btn-info waves-effect" type="button">
                                                        <i class="material-icons">border_color</i>
                                                    </button>
                                                </a>
                                                <button class="btn btn-danger waves-effect btn-delete" type="button">
                                                    <i class="material-icons">delete_forever</i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="thumbnail">
                                <div class="image view view-first">
                                    <img style="width: 100%; display: block;"
                                         src="{{ "/front/img/banners/{$a->id}.jpg" }}"
                                         alt="image"/>
                                </div>
                                <div class="caption">
                                    {{--<p>{{ $banner->description }}</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->

    {!! Form::open(['route' => ['advertising.destroy',':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('css')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>
@endsection
@section('js')
    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>
    @include('administration.templates.partials.delete_row2')
@endsection