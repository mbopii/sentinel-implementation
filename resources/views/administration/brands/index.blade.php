@extends('administration.templates.layout')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Marcas
                        <small>Seleccione una Marca para continuar o agregue una nueva en el menu</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{ route('brands.create') }}">Nuevo</a></li>
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        @if($brands->isEmpty())
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            No existen marcas Cargadas
                            <small>Agregue una nueva en el menu</small>
                        </h2>
                    </div>
                </div>
            </div>
        @else
            @foreach($brands as $brand)

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 dlt_card">
                    <div class="card">
                        <div class="card-image">
                            <img src="{{ "/admin/images/brands/{$brand->id}.jpg"  }}" width="100%" height="100%"
                                 alt="">
                        </div>
                        <div class="header">
                            <h2>
                                {{ $brand->description }}
                                <small></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                       role="button"
                                       aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" data-id="{{ $brand->id }}">
                                        <li>
                                            <a href="{{ route('brands.edit', $brand->id) }}">Editar</a>
                                        </li>
                                        <li><a href="#" class="btn-delete">Eliminar</a></li>
                                        {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            @endforeach
        @endif
    </div>

    {!! Form::open(['route' => ['brands.destroy', ':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('css')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/plugins/sweetalert/sweetalert.css' }}" rel="stylesheet"/>
@endsection
@section('js')
    <script src="{{ '/admin/plugins/sweetalert/sweetalert.min.js' }}"></script>
    @include('administration.templates.partials.delete_card')
@endsection