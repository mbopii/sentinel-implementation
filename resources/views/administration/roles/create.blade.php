@extends('administration.templates.layout')
@section("breadcrumbs")
    <h4 class="page-title">Roles</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Nuevo</li>
            </ol>
        </nav>
    </div>
@endsection
@section('body')
    {!! Form::open(['route' => ['roles.store', 'method' => 'post']]) !!}

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Nuevo Rol</h4>
                    @include("administration.roles.partials.fields")
                    <div class="border-top">
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Permisos</h4>
                    @include("administration.roles.partials.permissions")
                </div>
            </div>
        </div>
    {!! Form::close() !!}

@endsection