@extends('administration.templates.layout')
@section("breadcrumbs")
    <h4 class="page-title">Roles</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Lista</li>
            </ol>
        </nav>
    </div>
@endsection
@section('body')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Roles Cargados</h5>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Slug</th>
                        <th scope="col">Creado</th>
                        <th scope="col">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($roles as $role)
                    <tr data-id="{{ $role->id }}">
                        <th scope="row">{{ $role->id }}</th>
                        <td>{{ $role->description }}</td>
                        <td>{{ $role->slug}}</td>
                        <td>{{ $role->created_at->format("Y-m-d") }}</td>
                        <td>
                            <a href="{{ route('roles.edit', $role->id) }}">
                                <button class="btn btn-primary btn-sm">
                                    <i class="fa fa-pencil-alt"></i> Editar
                                </button>
                            </a>

                            <button class="btn btn-danger btn-sm btn-delete" type="button">
                                <i class="fa fa-times"></i> Eliminar
                            </button>
                        </td>
                    </tr>
                   @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => ['roles.destroy',':ROW_ID'], 'method' => 'DELETE',
                                  'id' => 'form-delete']) !!}
@endsection

@section('admin_css')
    <!-- Sweetalert Css -->
    <link href="{{ '/admin/assets/extra-libs/sweetalert/sweetalert2.css' }}" rel="stylesheet" />
@endsection

@section('admin_js')
    <script src="{{ '/admin/assets/extra-libs/sweetalert/sweetalert2.min.js' }}"></script>
    @include('administration.templates.partials.delete_row')
@endsection