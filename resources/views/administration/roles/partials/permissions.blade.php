<table class="table">
    <tr>
        @for($i = 0; $i < $permissions->count(); $i++)
            <td>
                <input type="checkbox" name="permissions[]" id="{{ $permissions[$i]->id }}"
                       class="filled-in" @if($permissions[$i]->has) checked
                       @endif value="{{ $permissions[$i]->permission }}"/>
                <br>
                <label for="{{ $permissions[$i]->id }}"> {{ $permissions[$i]->description }} </label>
            </td>
            @if(($i+1) % 4 == 0)
    </tr>
    <tr>
        @endif
        @endfor
    </tr>
</table>
