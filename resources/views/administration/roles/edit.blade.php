@extends('administration.templates.layout')
@section("breadcrumbs")
    <h4 class="page-title">Permisos</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Lista</li>
            </ol>
        </nav>
    </div>
@endsection
@section('body')
    {!! Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'patch']) !!}
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Editar Rol</h4>
                    @include("administration.roles.partials.fields")
                    <div class="border-top">
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Permisos</h4>
                    @include("administration.roles.partials.permissions")
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection