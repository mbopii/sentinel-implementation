<script type="text/javascript">
    // $(document).ready(function () {
    $('.btn-delete').click(function (e) {
        console.log("Delete a card - This can be fun");
        e.preventDefault();
        var row = $(this).parents('ul');
        var card = row.parent().parent();
        var id = row.data('id');

        console.log(card);
        swal({
                title: "Atención!",
                text: "Está a punto de borrar el registro, está seguro?.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, eliminar!",
                cancelButtonText: "No, cancelar!",
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
                closeOnCancel: true
            },

            function (isConfirm) {
                if (isConfirm) {
                    var form = $('#form-delete');
                    var url = form.attr('action').replace(':ROW_ID', id);
                    var data = form.serialize();
                    var type = "";
                    var title = "";
                    $.post(url, data, function (result) {

                        console.log(result.message);
                        if (result.error == false) {
                            card.fadeOut();
                            type = "success";
                            title = "Operación realizada!";
                        } else {
                            type = "error";
                            title = "No se pudo realizar la operación"
                        }
                        swal({title: title, text: result.message, type: type, confirmButtonText: "Aceptar"});
                    // swal({title: title, text: 'Success', type: type, confirmButtonText: "Aceptar"});
                    }).fail(function () {
                        swal('No se pudo realizar la petición.');
                    });
                }
            });
    });

    // });
</script>
