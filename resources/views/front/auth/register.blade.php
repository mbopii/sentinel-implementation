@extends('front.partials.layout')


@section('front_body_title')
    itemscope="" itemtype="http://schema.org/WebPage" class="templateCustomersRegister notouch"
@endsection

@section('front_body')
    <div id="content-wrapper">
        <!-- Content -->
        <div id="content" class="clearfix">
            <div id="breadcrumb" class="breadcrumb">
                <div itemprop="breadcrumb" class="container">
                    <div class="row">
                        <div class="col-md-24">
                            <a href="{{ route('index') }}" class="homepage-link"
                               title="Back to the frontpage">Inicio</a>
                            <span>/</span>
                            <span class="page-title">Registro</span>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-6">
                            <div id="page-header" class="col-md-24">
                                <h1 id="page-title">Registro</h1>
                            </div>

                            <div id="col-main" class="col-md-24 register-page clearfix">
                                <form method="post" action="{{ route('front.register.do') }}" id="create_customer"
                                      accept-charset="UTF-8">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input value="create_customer" name="form_type" type="hidden"><input name="utf8"
                                                                                                         value="✓"
                                                                                                         type="hidden">
                                    <ul class="row list-unstyled">
                                        <li id="emailf">
                                            <label class="control-label" for="first_name">Nombre y Apellido</label>
                                            <input name="description" id="first_name" class="form-control"
                                                   type="text">
                                        </li>
                                        <li id="emailf">
                                            <label class="control-label" for="first_name">Nombre de Usuario</label>
                                            <input name="username" id="first_name" class="form-control"
                                                   type="text">
                                        </li>
                                        <li class="clearfix"></li>
                                        <li id="emailf" class="">
                                            <label class="control-label" for="email">Email <span
                                                        class="req">*</span></label>
                                            <input name="email" id="email" class="form-control " type="email">
                                        </li>
                                        <li class="clearfix"></li>
                                        <li id="passwordf" class="">
                                            <label class="control-label" for="password">Contraseña <span
                                                        class="req">*</span></label>
                                            <input value="" name="password" id="password"
                                                   class="form-control password" type="password">
                                        </li>
                                        <li class="clearfix"></li>
                                        <li id="passwordf" class="">
                                            <label class="control-label" for="password">Confirmar Contraseña<span
                                                        class="req">*</span></label>
                                            <input value="" name="password_confirm" id="password"
                                                   class="form-control password" type="password">
                                        </li>
                                        <li class="clearfix"></li>
                                        <li class="unpadding-top action-last">
                                            <button class="btn" type="submit">Enviar</button>
                                        </li>
                                    </ul>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
