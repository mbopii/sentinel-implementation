@extends('front.partials.layout')

@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Home</a></li>
                            <li class="active"><a href="#">Inicio</a></li>
                        </ul>
                        <span class="crumb-name">Inicio de Sesion</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="my-account-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="personal-information">
                        <h4 class="my-account-title">Inicio de Sesión</h4>
                        <div class="personal-information-box">
                            <form action="{{ route('login') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Email</label>
                                            <input type="text" name="email">
                                        </div>
                                        <div class="pib-item">
                                            <label>Password</label>
                                            <input type="password" name="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-3 col-xs-12">
                                        <div class="pib-save-change">
                                            <button type="submit">Entrar</button>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="pib-save-change">
                                            <button id="facebook_button2"><i class="fa fa-facebook-square"></i> Entrar con Facebook</button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <br class="br_login">
                <div class="col-md-6">
                    <div class="personal-information">
                        <h4 class="my-account-title">Registro</h4>
                        <div class="personal-information-box">
                            <form action="{{ route('front.register.do') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="pib-item">
                                            <label>Email</label>
                                            <input type="text" name="email" required>
                                        </div>
                                        <div class="pib-item">
                                            <label>Nombre</label>
                                            <input type="text" name="description" required>
                                        </div>
                                        <div class="pib-item">
                                            <label>C.I</label>
                                            <input type="text" name="idnum" required>
                                        </div>
                                        <div class="pib-item">
                                            <label>Password</label>
                                            <input type="password" name="password" required>
                                        </div>
                                        <div class="pib-item">
                                            <label>Password</label>
                                            <input type="password" name="password_confirm" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pib-save-change">
                                            <button type="submit">Registrarme</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('front.js')
    <script>
        $('#facebook_button2').click(function (e) {
            console.log("Facebook Login");
            e.preventDefault();
            window.location.href = "{{ route('login.facebook') }}";
        });
    </script>
@endsection