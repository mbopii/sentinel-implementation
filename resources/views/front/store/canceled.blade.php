@extends('front.partials.layout')
@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Cancelar Orden</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- order-tracker-area-start -->
    <div class="order-tracker-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                    <div class="order-tracker">
                        <h2 class="ot-title">Orden Cancelada</h2>
                        <h6 class="ot-intro">Tu Orden fue Cancelada Exitosamente!</h6>
                        <div class="ot-input-box">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="track">
                                        <a href="{{ route('store.index') }}">
                                            <button class="hvr-bs">Volver a la tienda</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection