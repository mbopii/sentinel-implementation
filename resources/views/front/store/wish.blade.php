@extends('front.partials.layout')

@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Lista de Deseos</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- wishlist-table-area-start -->
    <div class="wishlist-table-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="wishlist-table-head">
                        <h4>Tu Lista de Deseos</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="wishlist-table shopping-cart-table table-responsive">
                        <table>
                            <thead>
                            <tr>
                                <th class="wishlist-pro-name">Producto</th>
                                <th class="wishlist-pro-desc">Marca</th>
                                <th class="wishlist-pro-name">Precio</th>
                                <th class="wishlist-pro-add-to">Al Carrito</th>
                                <th class="wishlist-pro-remove"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($wishListHeader as $wish)
                                <tr class="table-row1">
                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            <a href="#">{{ $wish->products->name }}</a>
                                        </p>
                                    </td>
                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            <a href="#">{{ $wish->products->brand->description }}</a>
                                        </p>
                                    </td>
                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            <a href="#">Gs. {{ number_format($wish->products->price, 0, ',', '.') }}</a>
                                        </p>
                                    </td>
                                    <td class="wishlist_add_cart">
                                        <input id="count" type="hidden" value="1" />
                                        <input id="product_id" type="hidden" value="{{ $wish->products->id }}" />
                                        <button id="send-button"><i class="fa fa-shopping-basket"></i></button>
                                    </td>
                                    <td class="wishlist_remove">
                                        <a href="{{ route('front.wish.remove', $wish->id) }}">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::open(['route' => ['cart.add',':ROW_ID', ':COUNT'], 'method' => 'GET',
                                        'id' => 'form-add']) !!} {!! Form::close() !!}
@endsection

@section('front.js')
    @include('front.partials.addcart')
@endsection