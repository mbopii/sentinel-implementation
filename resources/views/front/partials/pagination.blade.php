<div class="shop-pagination-area clearfix">
    <div class="pagination-box">
        <ul>
            @if ($paginator->hasPages())
                {{--<ul class="pager">--}}
                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                    <li><a href="#">&laquo;</a></li>
                @else
                    <li><a href="{{ $paginator->previousPageUrl() }}">&laquo;</a></li>
                @endif

                @if($paginator->currentPage() > 3)
                    <li class="active"><a href="{{ $paginator->url(1) }}">1</a></li>
                @endif
                @if($paginator->currentPage() > 4)
                    <li><a class="disabled hidden-xs"><span>...</span></a></li>
                @endif
                @foreach(range(1, $paginator->lastPage()) as $i)
                    @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
                        @if ($i == $paginator->currentPage())
                            <li class="active"><a>{{ $i }}</a></li>
                        @else
                            <li><a href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
                        @endif
                    @endif
                @endforeach
                @if($paginator->currentPage() < $paginator->lastPage() - 3)
                    <li><a><span>...</span></a></li>
                @endif
                @if($paginator->currentPage() < $paginator->lastPage() - 2)
                    <li><a href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a></li>
                @endif


                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li><a href="{{ $paginator->nextPageUrl() }}">&raquo;</a></li>
                @else
                    <li><a href="#">&raquo;</a></li>
                @endif
                {{--</ul>--}}
            @endif
        </ul>
    </div>
    {{--<p class="pagination-showing">Showing 1–20 of 60 results</p>--}}
</div>