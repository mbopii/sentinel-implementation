<footer>
    <div class="footer-top-area">
        <div class="container">
            <div class="footer-top">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="footer-desc">
                            <img src="/front/img/logo.png" alt="" width="20%", height="20%">
                            <p>Massari</p>
                            <div class="footer-desc-contact">
                                <h6>@lang('words.contact_title'): </h6>
                                <ul>
                                    <li><i class="fa fa-mobile"></i>+595 (21) 123 456</li>
                                    <li><i class="fa fa-envelope-o"></i><span>email: </span> contacto@massari.com.py</li>
                                    <li><i class="fa fa-map-marker"></i><span>dirección: </span>Mariscal Lopez, 123,
                                        Asunción, Paraguay</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="footer-links">
                            <h4 class="footer-title">Enlaces de Interes</h4>
                            <ul class="clearfix">
                                <li><a href="{{ route('index') }}">Inicio</a></li>
                                <li><a href="{{ route('front.about') }}">Nosotros</a></li>
                                <li><a href="{{ route('store.index') }}">Productos</a></li>
                                <li><a href="{{ route('login.page') }}">Inicio de sesion</a></li>
                                <li><a href="{{ route('login.page') }}">Registro</a></li>
                                <li><a href="#">Contacto</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="popular-tag">
                            <h4 class="footer-title">Busquedas Populares</h4>
                            <ul class="footer-tags">
                                <li><a href="{{ route('store.search', ['search' => 'perfume']) }}">Perfume</a></li>
                                <li><a href="{{ route('store.search', ['search' => 'rondina']) }}">Rondina</a></li>
                                <li><a href="{{ route('store.search', ['search' => 'cartera']) }}">Carteras</a></li>
                                <li><a href="{{ route('store.search', ['search' => 'calvin']) }}">Calvin Klein</a></li>
                                <li><a href="{{ route('store.search', ['search' => 'madres']) }}">Mamás</a></li>
                                <li><a href="{{ route('store.search', ['search' => 'peine'])}}">Peluquería</a></li>
                                <li><a href="#">Maquillaje</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area">
        <div class="container">
            <div class="footer-bottom">
                <div class="row">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="footer-social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="payment-method">
                            <ul>
                                <li><img src="{{ '/front/img/payment-method/american.png' }}" alt=""></li>
                                <li><img src="{{ '/front/img/payment-method/mastercard.png' }}" alt=""></li>
                                <li><img src="{{ '/front/img/payment-method/visa.png' }}" alt=""></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright">
                            <p>© 2018 Todos los Derechos Reservados. <a href="#" target="_blank">Massari</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>