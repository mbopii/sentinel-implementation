<script>
    $(document).ready(function () {
        $('#now-button').click(function (e) {
            e.preventDefault();
            console.log("Buy Now!");
            var quantity = $('#count').val();
            console.log(quantity);
            var product = $('#product_id').val();
            var form = $('#form-now');
            var url = form.attr('action').replace(':ROW_ID', product).replace(':COUNT', quantity);
            var data = form.serialize();
            var type = "";
            var title = "";
            $.get(url, data, function (result) {
                if (result.error == false) {
                    console.log(result);
                    var rootURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
                    var finalURL = rootURL + 'cart/checkout';
                    window.location.href = finalURL;
                } else {
                    $('#error').modal('show');
                    type = "error";
                    title = "Ocurrio un problema al intentar agregar su producto"
                }
            })

        });
    });
</script>
