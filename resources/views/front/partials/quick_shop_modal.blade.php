<script src="front/javascripts/cs.global.js" type="text/javascript"></script>

<div id="quick-shop-modal" class="modal in" role="dialog" aria-hidden="false" tabindex="-1" data-width="800">
    <div class="modal-backdrop in" style="height: 742px;">
    </div>
    <div class="modal-dialog rotateInDownLeft animated">
        <div class="modal-content">
            <div class="modal-header">
                <i class="close fa fa-times btooltip" data-toggle="tooltip" data-placement="top" title=""
                   data-dismiss="modal" aria-hidden="true" data-original-title="Close"></i>
            </div>
            <div class="modal-body">
                <div class="quick-shop-modal-bg" style="display: none;">
                </div>
                <div class="row">
                    <div class="col-md-12 product-image">
                        <div id="quick-shop-image" class="product-image-wrapper">
                            <a class="main-image"><img class="img-zoom img-responsive image-fly"
                                                       src="./front/images/1_grande.jpg"
                                                       data-zoom-image="./front/images/1.jpg" alt=""/></a>
                            <div id="gallery_main_qs" class="product-image-thumb">
                                <a class="image-thumb active" href="./front/1images/.jpg"
                                   data-image="./front/images/1_grande.jpg" data-zoom-image="./front/images/1.jpg"><img
                                            src="./front/images/1_compact.jpg" alt=""/></a>
                                <a class="image-thumb" href="./front/images/2.jpg"
                                   data-image="./front/images/2_grande.jpg" data-zoom-image="./front/images/2.jpg"><img
                                            src="./front/images/2_compact.jpg" alt=""/></a>
                                <a class="image-thumb" href="./front/images/3.jpg"
                                   data-image="./front/images/3_grande.jpg" data-zoom-image="./front/images/3.jpg"><img
                                            src="./front/images/3_compact.jpg" alt=""/></a>
                                <a class="image-thumb" href="./front/images/4.jpg"
                                   data-image="./front/images/4_grande.jpg" data-zoom-image="./front/images/4.jpg"><img
                                            src="./front/images/4_compact.jpg" alt=""/></a>
                                <a class="image-thumb" href="./front/images/5.jpg"
                                   data-image="./front/images/5_grande.jpg" data-zoom-image="./front/images/5.jpg"><img
                                            src="./front/images/5_compact.jpg" alt=""/></a>
                                <a class="image-thumb" href="./front/images/6.jpg"
                                   data-image="./front/images/6_grande.jpg" data-zoom-image="./front/images/6.jpg"><img
                                            src="./front/images/6_compact.jpg" alt=""/></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 product-information">
                        <h1 id="quick-shop-title"><span> <a href="/products/curabitur-cursus-dignis">Curabitur cursus dignis</a></span>
                        </h1>
                        <div id="quick-shop-infomation" class="description">
                            <div id="quick-shop-description" class="text-left">
                                <p>
                                    Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo
                                    minus id quod maxime placeat facere possimus, omnis amet voluptas assumenda est,
                                    omnis dolor repellendus quis nostrum.
                                </p>
                                <p>
                                    Temporibus autem quibusdam et aut officiis debitis aut rerum dolorem necessitatibus
                                    saepe eveniet ut et neque porro quisquam est, qui dolorem ipsum quia dolor s...
                                </p>
                            </div>
                        </div>
                        <div id="quick-shop-container">
                            <div id="quick-shop-relative" class="relative text-left">
                                <ul class="list-unstyled">
                                    <li class="control-group vendor">
                                        <span class="control-label">Vendor :</span><a
                                                href="/collections/vendors?q=Vendor+1"> Vendor 1</a>
                                    </li>
                                    <li class="control-group type">
                                        <span class="control-label">Type :</span><a
                                                href="/collections/types?q=Sweaters+Wear"> Sweaters Wear</a>
                                    </li>
                                </ul>
                            </div>
                            <form action="#" method="post" class="variants" id="quick-shop-product-actions"
                                  enctype="multipart/form-data">
                                <div id="quick-shop-price-container" class="detail-price">
                                    <span class="price_sale">$259.00</span><span class="dash">/</span>
                                    <del class="price_compare">$300.00</del>
                                </div>
                                <div class="quantity-wrapper clearfix">
                                    <label class="wrapper-title">Quantity</label>
                                    <div class="wrapper">
                                        <input type="text" id="qs-quantity" size="5" class="item-quantity"
                                               name="quantity" value="1">
                                        <span class="qty-group">
											<span class="qty-wrapper">
											<span class="qty-up" title="Increase" data-src="#qs-quantity">
											<i class="fa fa-plus"></i>
											</span>
											<span class="qty-down" title="Decrease" data-src="#qs-quantity">
											<i class="fa fa-minus"></i>
											</span>
											</span>
											</span>
                                    </div>
                                </div>
                                <div id="quick-shop-variants-container" class="variants-wrapper">
                                    <div class="selector-wrapper">
                                        <label for="#quick-shop-variants-1293238211-option-0">Color</label>
                                        <div class="wrapper">
                                            <select class="single-option-selector" data-option="option1"
                                                    id="#quick-shop-variants-1293238211-option-0"
                                                    style="z-index: 1000; position: absolute; opacity: 0; font-size: 15px;">
                                                <option value="black">black</option>
                                                <option value="red">red</option>
                                                <option value="blue">blue</option>
                                                <option value="purple">purple</option>
                                                <option value="green">green</option>
                                                <option value="white">white</option>
                                            </select>
                                            <button type="button" class="custom-style-select-box"
                                                    style="display: block; overflow: hidden;"><span
                                                        class="custom-style-select-box-inner"
                                                        style="width: 264px; display: inline-block;">black</span>
                                            </button>
                                            <i class="fa fa-caret-down"></i>
                                        </div>
                                    </div>
                                    <div class="selector-wrapper">
                                        <label for="#quick-shop-variants-1293238211-option-1">Size</label>
                                        <div class="wrapper">
                                            <select class="single-option-selector" data-option="option2"
                                                    id="#quick-shop-variants-1293238211-option-1"
                                                    style="z-index: 1000; position: absolute; opacity: 0; font-size: 15px;">
                                                <option value="small">small</option>
                                                <option value="medium">medium</option>
                                                <option value="large">large</option>
                                            </select>
                                            <button type="button" class="custom-style-select-box"
                                                    style="display: block; overflow: hidden;"><span
                                                        class="custom-style-select-box-inner"
                                                        style="width: 264px; display: inline-block;">small</span>
                                            </button>
                                            <i class="fa fa-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="others-bottom">
                                    <input id="quick-shop-add" class="btn small add-to-cart" type="submit" name="add"
                                           value="Add to Cart" style="opacity: 1;">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>