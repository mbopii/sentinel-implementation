@extends('front.partials.layout')

@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Mi Cuenta</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- wishlist-table-area-start -->
    <div class="wishlist-table-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="wishlist-table-head">
                        <h4>Tu Compras - Orden Nro {{ $ordersHeader->id }}</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="wishlist-table shopping-cart-table table-responsive">
                        <table>
                            <thead>
                            <tr>
                                <th class="wishlist-pro-name">Id</th>
                                <th class="wishlist-pro-desc">Producto</th>
                                <th class="wishlist-pro-availabillity">Cant.</th>
                                <th class="wishlist-pro-name">Precio</th>
                                <th class="wishlist-pro-desc">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($ordersHeader->details as $detail)
                                <tr class="table-row1">
                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            <a href="#">{{ $detail->product->id }}</a>
                                        </p>
                                    </td>
                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            <a href="#">{{ $detail->product->name }}</a>
                                        </p>
                                    </td>
                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            <a href="#">{{ $detail->quantity }}</a>
                                        </p>
                                    </td>

                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            <a href="#">Gs. {{ number_format($detail->product->price, 0, ',', '.') }}</a>
                                        </p>
                                    </td>
                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            @if($detail->discount)
                                                <a href="#">Gs. {{ number_format($detail->sub_total - $detail->total_discount, 0, ',', '.') }}</a>
                                                <a href="#"><strike>Gs. {{ number_format($detail->sub_total, 0, ',', '.') }}</strike></a>
                                            @else
                                                <a href="#">Gs. {{ number_format($detail->sub_total, 0, ',', '.') }}</a>
                                            @endif
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr class="table-row1">
                                <td class="cart_product_name">&nbsp;</td>
                                <td class="cart_product_name">&nbsp;</td>
                                <td class="cart_product_name_value"><p class="cart_product_name">Total</p></td>
                                <td class="cart_product_quantity_value"><span
                                            class="product-quantity-t">&nbsp;</span></td>
                                <td class="cart_product_total_value">
                                    @if ($ordersHeader->discount)
                                        <span
                                                class="product_price">Gs. {{ number_format($ordersHeader->order_amount - $ordersHeader->discount_amount, 0, ',', '.') }}</span>
                                        <span
                                                class="product_price"><strike>Gs. {{ number_format($ordersHeader->order_amount, 0, ',', '.') }}</strike></span>

                                    @else
                                        <span
                                                class="product_price">Gs. {{ number_format($ordersHeader->order_amount, 0, ',', '.') }}</span>
                                    @endif
                                </td>
                                <td class="cart_product_name">&nbsp;</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            @if ($ordersHeader->payment_method_id == 3)
                <div class="row">
                    <div class="col-xs-12">
                        <div class="scca-head">
                            <form action="{{ route('front.users.purchases.upload', $ordersHeader->id) }}"
                                  method="POST" enctype="multipart/form-data">
                                <input type="file" name="invoice_file">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="scb-continue shopping-cart-button hvr-bs"
                                        href="{{ route('store.index') }}">
                                    Subir Imagen
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            @endif


        </div>
    </div>
@endsection

