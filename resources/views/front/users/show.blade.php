@extends('front.partials.layout')


@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Mi Cuenta</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- my-account-area-start -->
    <div class="my-account-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="my-account-welcome">
                        <h4 class="my-account-title">Hola {{ $user->description }}!</h4>
                        <ul class="profile">
                            <li><a href="{{ route('front.user.show', $user->id) }}"><i class="fa fa-user"
                                                                                       style="color:#662B2D"></i> Mis
                                    Datos</a></li>
                            <li><a href="{{ route('front.user.addresses', $user->id) }}"><i
                                            class="fa fa-address-book"></i> Mis Direcciones</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="personal-information">
                        <h4 class="my-account-title">Mis Datos</h4>
                        <div class="personal-information-box">
                            <div class="row">
                                {!! Form::model($user, ['route' => ['front.user.update', $user->id], 'files' => 'true', 'method' => 'patch']) !!}
                                <div class="col-md-6 col-xs-12">
                                    <div class="pib-item">
                                        <label>Nombre</label>
                                        {!! Form::text('description', null, ['placeholder' => 'Ingresa tu nombre ...', 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="pib-item">
                                        <label>Email</label>
                                        {!! Form::text('email', null, ['placeholder' => 'Ingresa tu correo ...', 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="pib-item">
                                        <label>Contrasena</label>
                                        {!! Form::password('password', ['placeholder' => 'Ingresa tu contrasena ...']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="pib-item">
                                        <label>Confirmar Contrasena</label>
                                        {!! Form::password('password_confirm', ['placeholder' => 'Confirma tu contrasena ...']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="pib-item">
                                        <label>Razon Social</label>
                                        {!! Form::text('tax_name', null, ['placeholder' => 'Ingresa tu Razon Social ...', 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="pib-item">
                                        <label>Ruc</label>
                                        {!! Form::text('tax_code', null, ['placeholder' => 'Ingresa tu Ruc ...', 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="pib-item">
                                        <label>Teléfono</label>
                                        {!! Form::text('telephone', null, ['placeholder' => 'Ingresa tu Teléfono ...', 'required']) !!}
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12">
                                    <div class="pib-item">
                                        <label>Cédula</label>
                                        {!! Form::text('idnum', null, ['placeholder' => 'Ingresa tu cedula de identidad ...', 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="pib-item pib-item-date">
                                        <label>Fecha de Nacimiento</label>
                                        {!! Form::text('birthday', null, ['placeholder' => 'Ingresa tu fecha de nacimiento ...', 'required']) !!}
                                    </div>
                                </div>


                                <div class="col-xs-12">
                                    <div class="pib-save-change">
                                        <button type="submit">Guardar Cambios</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #element_to_pop_up {
            background-color: #fff;
            border-radius: 10px 10px 10px 10px;
            box-shadow: 0 0 25px 5px #999;
            color: #111;
            display: none;
            min-width: 450px;
            padding: 25px;
            font-family: 'Montserrat', sans-serif;
            font-weight: normal;
        }

        .b-close {
            border-radius: 7px 7px 7px 7px;
            box-shadow: none;
            font: bold 131% sans-serif;
            padding: 0 6px 2px;
            position: absolute;
            right: -7px;
            top: -7px;
            background-color: #662B2D;
            color: #fff;
            cursor: pointer;
            display: inline-block;

        }

        .popup_logo {
            color: #662B2D;
            font: bold 325% 'Montserrat', sans-serif;
        }

    </style>
    <div id="element_to_pop_up"
         style="left: 268.5px;position: absolute;top: 215.5px;z-index: 9999;opacity: 1;display: none;">
        <span class="button b-close"><span>X</span></span>
        <span class="popup_logo">Exito</span><br><br>
        Datos modificados exitosamente!<br><br>
    </div>

    <!-- my-account-area-end -->
@endsection
@section ('js')
    <script>
        $('#user_popup').bPopup();
    </script>


@endsection