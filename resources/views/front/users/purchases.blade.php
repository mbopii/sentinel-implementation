@extends('front.partials.layout')

@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Tus Compras</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- wishlist-table-area-start -->
    <div class="wishlist-table-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="wishlist-table-head">
                        <h4>Tu Compras</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="wishlist-table table-responsive shopping-cart-table">
                        <table>
                            <thead>
                            <tr>
                                <th class="wishlist-pro-name table_not_show">Orden</th>
                                <th class="wishlist-pro-desc">Fecha</th>
                                <th class="wishlist-pro-availabillity">Forma de Pago</th>
                                <th class="wishlist-pro-add-to">Total</th>
                                <th class="wishlist-pro-add-to">Detalles</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($ordersHeader as $header)
                                <tr class="table-row1" id="{{ $header->id }}" @if($header->status_id == 1)style="background-color: #ef8383 !important;"@endif>
                                    <td class="cart_product_name_value table_not_show" style="width: 20px!important;">
                                        <p class="wishlist_product_name">
                                            <a href="#">{{ $header->id }}</a>
                                        </p>
                                    </td>
                                    <td class="cart_product_name_value" width="25%">
                                        <p class="wishlist_product_name">
                                            <a href="#">{{ $header->purchase_date }}</a>
                                        </p>
                                    </td>

                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            <a href="#">{{ $header->paymentForm->description }}</a>
                                        </p>
                                    </td>
                                    <td class="cart_product_name_value">
                                        <p class="wishlist_product_name">
                                            <a href="#">Gs. {{ number_format($header->order_amount, 0, ',', '.') }}</a>
                                        </p>
                                    </td>
                                    <td class="wishlist_add_cart">
                                        <input type="hidden" name="order_id" value="{{ $header->id }}">
                                        <button class="details_button"><i class="fa fa-search" title="Ver detalles"></i>
                                        </button>
                                        @if($header->aex_tracking_id)
                                            <button class="tracking_button"><i class="fa fa-truck"
                                                                               title="Tracking de Pedido"></i></button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('front.js')
    <script>
        $('.details_button').click(function (e) {
            e.preventDefault();
            window.location.href = "https://massari.com.py/purchases/" + $(this).parents('tr')[0].id + "/details";
        });

        $('.tracking_button').click(function (e) {
            e.preventDefault();
            window.location.href = "https://massari.com.py/purchases/" + $(this).parents('tr')[0].id + "/tracking";
        });
    </script>
@endsection