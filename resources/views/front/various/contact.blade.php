@extends('front.partials.layout')

@section('front_body_title')
    itemscope="" itemtype="http://schema.org/WebPage" class="templateCollection notouch"
@endsection
@section('front_body')
    <div id="content-wrapper">
        <div id="content-wrapper">
            <!-- Content -->
            <div id="content" class="clearfix">
                <div id="breadcrumb" class="breadcrumb">
                    <div itemprop="breadcrumb" class="container">
                        <div class="row">
                            <div class="col-md-24">
                                <a href="{{ route('index') }}" class="homepage-link" title="Back to the frontpage">Home</a>
                                <span>/</span>
                                <span class="page-title">Contacto</span>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="content">
                    <div class="container">
                        <div class="row">
                            <div id="page-header">
                                <h1 id="page-title">Contactanos</h1>
                            </div>
                        </div>
                    </div>
                    <div id="col-main" class="contact-page clearfix">
                        <div class="group-contact clearfix">
                            <div class="container">
                                <div class="row">
                                    <div class="left-block col-md-12">
                                        <form method="post" action="/contact" class="contact-form" accept-charset="UTF-8">
                                            <input type="hidden" value="contact" name="form_type"><input type="hidden" name="utf8" value="✓">
                                            <ul id="contact-form" class="row list-unstyled">
                                                <li class="">
                                                    <h3>&nbsp;</h3>
                                                </li>
                                                <li class="">
                                                    <label class="control-label" for="name">Nombre</label>
                                                    <input type="text" id="name" value="" class="form-control" name="name">
                                                </li>
                                                <li class="clearfix"></li>
                                                <li class="">
                                                    <label class="control-label" for="email">Email <span class="req">*</span></label>
                                                    <input type="email" id="email" value="" class="form-control email" name="email">
                                                </li>
                                                <li class="clearfix"></li>
                                                <li class="">
                                                    <label class="control-label" for="message">Mensaje <span class="req">*</span></label>
                                                    <textarea id="message" rows="5" class="form-control" name="message"></textarea>
                                                </li>
                                                <li class="clearfix"></li>
                                                <li class="unpadding-top">
                                                    <button type="submit" class="btn">Enviar</button>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                    <div class="right-block contact-content col-md-12">
                                        <h6 class="sb-title"><i class="fa fa-home"></i> Donde estamos</h6>
                                        <ul class="right-content">
                                            <li class="title">
                                                <h6>Nuestro Local</h6>
                                            </li>
                                            <li class="address">
                                                <p>
                                                    Rca de Colombia 295 esq. Iturbe (5.49 mi)
                                                    Asunción, Paraguay
                                                </p>
                                            </li>
                                            <li class="phone">0981 534290</li>
                                            <li class="email"><i class="fa fa-envelope"></i> contacto@astral.com.py</li>
                                        </ul>
                                        <ul class="right-content">
                                            <li class="title">
                                                <h6>Seguinos en </h6>
                                            </li>
                                            <li class="facebook"><a href="#"><span class="fa-stack fa-lg btooltip" title="" data-original-title="Facebook"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-inverse fa-stack-1x"></i></span></a></li>
                                            <li class="twitter"><a href="#"><span class="fa-stack fa-lg btooltip" title="" data-original-title="Twitter"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-inverse fa-stack-1x"></i></span></a></li>
                                            <li class="google-plus"><a href="#"><span class="fa-stack fa-lg btooltip" title="" data-original-title="Google plus"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-google-plus fa-inverse fa-stack-1x"></i></span></a></li>
                                            <li class="pinterest"><a href="#"><span class="fa-stack fa-lg btooltip" title="" data-original-title="Pinterest"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-pinterest fa-inverse fa-stack-1x"></i></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {{--<div id="contact_map_wrapper">--}}
                                {{--<div id="contact_map" class="map"></div>--}}
                                {{--<script>--}}
                                    {{--jQuery(document).ready(function($) {--}}
                                        {{--if(jQuery().gMap){--}}
                                            {{--if($('#contact_map').length){--}}
                                                {{--$('#contact_map').gMap({--}}
                                                    {{--zoom: 17,--}}
                                                    {{--scrollwheel: false,--}}
                                                    {{--maptype: 'ROADMAP',--}}
                                                    {{--markers:[--}}
                                                        {{--{--}}
                                                            {{--address: '249 Ung Văn Khiêm, phường 25, Ho Chi Minh City, Vietnam',--}}
                                                            {{--html: '_address'--}}
                                                        {{--}--}}
                                                    {{--]--}}
                                                {{--});--}}
                                            {{--}--}}
                                        {{--}--}}
                                    {{--});--}}
                                {{--</script>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </section>
            </div>
        </div>
@endsection