@extends('front.partials.layout')

@section('front_body')
    <!-- crumb-area-start -->
    <div class="crumb-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="crumb">
                        <ul class="crumb-list">
                            <li><a href="{{ route('index') }}">Inicio</a></li>
                            <li class="active"><a href="#">Nosotros</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- crumb-area-end -->
    <!-- about-us-area-start -->
    <div class="about-us-area">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-12 col-xs-12">
                    <div class="some-words-img">
                        <img src="{{ '/front/img/about-us/banner.jpg' }}" alt="">
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="some-words-content">
                        <h2 class="about-us-title">Quienes Somos?</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum.</p>
                        <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterar</p>
                        <p>Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="feature-visual">
                        <h2 class="about-us-title">Que ofrecemos?</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                        <ul>
                            <li>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat;</li>
                            <li>Nam liber tempor cum soluta nobis eleifend option;</li>
                            <li>Claritas est etiam processus dynamicus, qui sequitur mutationem;</li>
                            <li>Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum;</li>
                            <li>Nam liber tempor cum soluta nobis eleifend.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="why-our-shop">
                        <h2 class="about-us-title">Porque Comprar con Nosotros?</h2>
                        <div class="why-accordion">
                            <button class="accordion">Lorem ipsum dolor sit amet</button>
                            <div class="panel">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>
                            </div>
                            <button class="accordion">Ut wisi enim ad minim veniam, quis nostrud</button>
                            <div class="panel">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>
                            </div>
                            <button class="accordion">Duis autem vel eum iriure dolor</button>
                            <div class="panel">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>
                            </div>
                            <button class="accordion">Typi non habent claritatem insitam</button>
                            <div class="panel">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>
                            </div>
                            <button class="accordion">Mirum est notare quam littera gothica quam</button>
                            <div class="panel">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- about-us-area-end -->
@endsection