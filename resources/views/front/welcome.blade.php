@extends('front.partials.layout')

@section('front_body')
    <!-- home1-slider-start -->
    <div class="home1-slider-container">
        <!-- Slider Image -->
        <div id="home1_slider" class="nivoSlider slider-image">
            <img src="front/img/slider/slider1.jpg" alt="" title="#htmlcaption1"/>
            <img src="front/img/slider/slider2.jpg" alt="" title="#htmlcaption2"/>
        </div>
        <!-- Slider Caption 1 -->
        <div id="htmlcaption1" class="nivo-html-caption slider-caption-1">
            <div class="slider-progress"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-md-offset-1 col-sm-10 col-sm-offset-1">
                        <div class="slide1-text hidden-xs">
                            <div class="middle-text">
                                <div class="cap-dec animated bounceIn">
                                    <h3>Todo para la</h3>
                                </div>
                                <div class="cap-title animated zoomIn">
                                    <h2><span>MUJER</span> de hoy </h2>
                                </div>
                                <div class="cap-details animated bounceInRight">
                                    <p>Todo para la mujer de hoy, productos recien llegados solo para vos</p>
                                </div>
                                <div class="cap-readmore animated bounceInUp">
                                    <a class="hvr-bs" href="{{ route('store.index') }}">Ver Productos</a>
                                </div>
                                <div class="cap-outer-text">
                                    <p>Massari <span>2018</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider Caption 2 -->
        <div id="htmlcaption2" class="nivo-html-caption slider-caption-2">
            <div class="slider-progress"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-md-offset-1 col-sm-10 col-sm-offset-1">
                        <div class="slide1-text hidden-xs">
                            <div class="middle-text">
                                <div class="cap-dec animated bounceIn">
                                    <h3>Todo para la</h3>
                                </div>
                                <div class="cap-title animated zoomIn">
                                    <h2>MUJER <span>2018</span></h2>
                                </div>
                                <div class="cap-details animated bounceInRight">
                                    <p>Productos encargados exclusivamente para vos</p>
                                </div>
                                <div class="cap-readmore animated bounceInUp">
                                    <a class="hvr-bs" href="{{ route('store.index') }}">Ver Productos</a>
                                </div>
                                <div class="cap-outer-text">
                                    <p>Massari <span>2018</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- home1-slider-end -->
    <!-- top-banner-area-start -->
    <div class="top-banner-area">
        <div class="container">
            <div class="featured-item-head">
                <div class="row">
                    <div class="col-md-5 col-sm-6 col-xs-12">
                        <div class="featured-item-left">
                            <div class="section-title">
                                <span>Massari</span>
                                <h2>Productos Destacados</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="featured-items tab-content">
                <div id="all" class="tab-pane fade in active">
                    <div class="featured-all">
                        <div class="fi-row">
                            <div class="row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="brands" style="padding-bottom: 0px !important;">
                                                <div class="products-carousel owl-carousel">
                                                    @foreach ($featuredProducts as $product)
                                                            <div class="single-featured-item item" style="padding-left: 10px; padding-right: 10px">
                                                                <div class="sfi-img">
                                                                    <a href="{{ route('store.show', $product->id) }}"><img style="height: 345px !important;"  src="{{ "/front/img/products/{$product->id}/1.jpg" }}" alt=""></a>
                                                                    <div class="sfi-img-content">
                                                                        <ul class="clearfix">
                                                                            <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                                                            <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                                                                            <li><a href="{{ route('store.show', $product->id) }}"><i
                                                                                            class="fa fa-eye"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="sfi-content">
                                                                    <div class="sfi-name-cat">
                                                                        <a class="sfi-name" href="#">{{ strlen($product->name) > 20 ? substr($product->name, 0, 20) . "..." : $product->name }}</a>
                                                                        <span class="sfi-cat">{{ $product->categories->description }}</span>
                                                                    </div>
                                                                    <div class="sfi-price-rating">
                                                                        <p class="sfi-price">Precio: <span>Gs. {{ number_format($product->price, 0, ',', '.') }}</span></p>
                                                                        <div class="rating">
                                                                            {{--<div class="star star-on"></div>--}}
                                                                            {{--<div class="star star-on"></div>--}}
                                                                            {{--<div class="star star-on"></div>--}}
                                                                            {{--<div class="star star-on"></div>--}}
                                                                            {{--<div class="star star-half"></div>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    @endForeach
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- top-banner-area-end -->
    <!-- featured-itema-area-start -->
    <div class="featured-item-area">

        <div class="container">
            <div class="featured-item-head">
                <div class="row">
                    <div class="col-md-5 col-sm-6 col-xs-12">
                        <div class="featured-item-left">
                            <div class="section-title">
                                <span>Nuestas</span>
                                <h2>Categorías</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($listCategories as $category)
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="single-top-banner">
                            <div class="stb-img">
                                <a href="{{ route('store.filter.category', $category->id) }}"><img
                                            src="front/img/top-banner/banner{{$category->id}}.jpg" alt="" style="height: 345px !important;"></a>
                            </div>
                            <div class="stb-content">
                                {{--<h4 class="stb-title2">collection</h4>--}}
                                <h2 class="stb-title">{{ $category->description }}</h2>
                                <p class="stb-text">Lorem Ipsum is simply dummy text of</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- featured-itema-area-end -->
    <!-- special-offer-area-start -->

    <!-- special-offer-area-end -->
    <!-- top-sale-trend-area-start -->
    <!-- top-sale-trend-area-end -->
    <!-- feature-post-area-start -->
    <!-- feature-post-area-end -->
    <!-- service-area-start -->
    <br>
    <div class="service-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="single-service customer-service">
                        <div class="single-service-icon">
                            <img src="front/img/service/customer.png" alt="">
                        </div>
                        <h4 class="ss-title">Atención 24/7</h4>
                        <p class="ss-desc">Atencion Personalizada 24/7</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="single-service customer-service">
                        <div class="single-service-icon">
                            <img src="front/img/service/money-back.png" alt="">
                        </div>
                        <h4 class="ss-title">GARANTÍA DE LOS PRODUCTOS !</h4>
                        <p class="ss-desc">Lorem ipsum </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="single-service customer-service">
                        <div class="single-service-icon">
                            <img src="front/img/service/shipping.png" alt="">
                        </div>
                        <h4 class="ss-title">DELIVERY GRATIS</h4>
                        <p class="ss-desc">para Asunción y Gran Asunción</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- service-area-end -->
    <!-- newsletter-area-start -->
    {{--<div class="newsletter-area">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-6 col-md-offset-3 col-xs-12">--}}
    {{--<div class="newsletter-content">--}}
    {{--<h2>Lista de Noticias</h2>--}}
    {{--<p>Suscribite a nuestra lista de noticias para enterarte de las últimas novedades!.</p>--}}
    {{--<div class="newsletter-form">--}}
    {{--<form action="#">--}}
    {{--<input type="email" placeholder="Ingresá tu Email" name="EMAIL">--}}
    {{--<input class="hvr-bs" type="submit" value="Suscribirme!" name="subscribe">--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <!-- newsletter-area-end -->
    <!-- brand-area-start -->
    <div class="brand-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="brands">
                        <div class="brand-carousel owl-carousel">
                            <div class="brand-item">
                                <img src="front/img/brand/brand3.png" alt="">
                            </div>
                            <div class="brand-item">
                                <img src="front/img/brand/brand1.png" alt="">
                            </div>
                            <div class="brand-item">
                                <img src="front/img/brand/brand4.png" alt="">
                            </div>
                            <div class="brand-item">
                                <img src="front/img/brand/brand5.png" alt="">
                            </div>
                            <div class="brand-item">
                                <img src="front/img/brand/brand2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection