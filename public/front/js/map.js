function myMap(id, preCoordinates){
    mapboxgl.accessToken = 'pk.eyJ1IjoiYWNuZXIiLCJhIjoiY2o3ODVqYjB6MjhjMDM1cGFoOHFucG15ZiJ9.7Wqebd8nU9fE0O4-REYdJw';



// Holds mousedown state for events. if this
// flag is active, we move the point on `mousemove`.
    var isDragging;

// Is the cursor over a point? if this
// flag is active, we listen for a mousedown event.
    var isCursorOverPoint;

    if (preCoordinates == null){
        console.log("This is a null value");
        var coordinates = document.getElementById('coordinates'+id);
    }else{
        console.log("Hire we get the coordinates value");
        var coordinates = preCoordinates;
    }


    console.log(id);
    console.log(coordinates);
    var map = new mapboxgl.Map({
        container: id,
        style: 'mapbox://styles/mapbox/streets-v9',
        center: [-57.6217949041815, -25.30160508698569],
        zoom: 13
    });





    var canvas = map.getCanvasContainer();

    var geojson = {
        "type": "FeatureCollection",
        "features": [

            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [ -57.6217949041815, -25.30160508698569]
                }
            }]
    };




    function mouseDown() {
        if (!isCursorOverPoint) return;

        isDragging = true;

        // Set a cursor indicator
        canvas.style.cursor = 'grab';

        // Mouse events
        map.on('mousemove', onMove);
        map.once('mouseup', onUp);
    }

    function onMove(e) {
        if (!isDragging) return;
        var coords = e.lngLat;

        // Set a UI indicator for dragging.
        canvas.style.cursor = 'grabbing';

        // Update the Point feature in `geojson` coordinates
        // and call setData to the source layer `point` on it.
        geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
        map.getSource('point').setData(geojson);
    }

    function onUp(e) {
        if (!isDragging) return;
        var coords = e.lngLat;

        // Print the coordinates of where the point had
        // finished being dragged to on the map.
        coordinates.style.display = 'block';
        coordinates.innerHTML = 'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat;
        canvas.style.cursor = '';
        isDragging = false;

        // Unbind mouse events
        map.off('mousemove', onMove);
    }

    map.on('load', function() {

        map.loadImage('/front/img/marker.png', function(error, image) {
            if (error) throw error;
            map.addImage('imagencita', image);
            map.addLayer({
                "id": "point",
                "type": "symbol",
                "source": {
                    "type": "geojson",
                    "data": geojson
                },
                "layout": {
                    "icon-image": "imagencita",
                    "icon-size": 0.30
                },
                "filter": ["==", "$type", "Point"]
            });






            // When the cursor imagencita a feature in the point layer, prepare for dragging.
            map.on('mouseenter', 'point', function() {
                // map.setPaintProperty('point', 'circle-color', '#ff0000');
                canvas.style.cursor = 'move';
                isCursorOverPoint = true;
                map.dragPan.disable();
            });

            map.on('mouseleave', 'point', function() {
                //map.setPaintProperty('point', 'circle-color', '#ff0000');
                canvas.style.cursor = '';
                isCursorOverPoint = false;
                map.dragPan.enable();
            });

            map.on('mousedown', mouseDown);
        });
        map.on('mouseup', function (e) {
            console.log('this is mapparta!!!');
            var latitude = $('#latitude-' + id);
            var longitude = $('#longitude-' + id);
            console.log(latitude);
            console.log(longitude);
            longitude.attr('value', e.lngLat.lng);
            latitude.attr('value', e.lngLat.lat);
            console.log(e.lngLat.lng);
            map.on('mousedown', mouseDown);
        });

    });

}