<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('administration')->group(function () {
    /*
    |--------------------------------------------------------------------------
    | Permission Routes | Sentinel Implementation
    |--------------------------------------------------------------------------
    */
    Route::resource('permissions', 'Admin\PermissionsController');

    /*
    |--------------------------------------------------------------------------
    | Roles Routes | Sentinel Implementation
    |--------------------------------------------------------------------------
    */
    Route::resource('roles', 'Admin\RolesController');

    /*
    |--------------------------------------------------------------------------
    | Users Routes | Sentinel Implementation
    |--------------------------------------------------------------------------
    */
    Route::post('users/admin/store', ['as' => 'users.admin.store', 'uses' => 'Admin\UsersController@saveAdminUser']);
    Route::get('users/search/{idnum}', 'Admin\UsersController@searchClient');
    Route::resource('users', 'Admin\UsersController');

    /*
    |--------------------------------------------------------------------------
    | Login Routes | Sentinel Implementation
    |--------------------------------------------------------------------------
    */
    Route::get('login', ['as' => 'admin.login.page', 'uses' => 'Auth\LoginController@adminLoginPage']);
    Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@loginAttempt']);


    /*
    |--------------------------------------------------------------------------
    | Departments Routes
    |--------------------------------------------------------------------------
    */
    Route::resource('departments', 'Admin\DepartmentsController');

    /*
    |--------------------------------------------------------------------------
    | Cities Routes
    |--------------------------------------------------------------------------
    */
    Route::resource('departments/{department_id}/cities', 'Admin\CitiesController');

    /*
    |--------------------------------------------------------------------------
    | Admin Home Routes
    |--------------------------------------------------------------------------
    */
    Route::get('/', ['as' => 'admin.index', 'uses' => 'Admin\AdminHomeController@index']);

});

/*
|--------------------------------------------------------------------------
| Front Various Index
|--------------------------------------------------------------------------
*/
Route::get('about', ['as' => 'front.about', 'uses' => 'Front\HomeController@aboutUs']);
Route::get('contact', ['as' => 'front.contact.show', 'uses' => 'Front\HomeController@showContact']);
Route::get('/', ['as' => 'index', 'uses' => 'Front\HomeController@index']);
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'Front\HomeController@changeLanguage']);