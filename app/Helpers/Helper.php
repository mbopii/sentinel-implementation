<?php

/**
 * Get all categories for the store
 */
function getArrayCategories()
{
    $categories = \App\Models\Categories::orderBy('id', 'asc')->pluck('description', 'id')->toArray();
    $categories = [0 => 'Seleccione ..'] + $categories;

    return $categories;
}

/*
 * Get all categories for the navbar
 */
function getAllCategories()
{
    $categories = \App\Models\Categories::orderBy('id', 'asc')->get();
    $navBarCategories = [];
    $i = 0;
    $flag = 0;

    foreach ($categories as $category) {
        if ($i == 4) {
            $flag++;
            $i = 0;
        }
        $navBarCategories[$flag][$i] = $category;
        $i++;
    }

    return $navBarCategories;
}