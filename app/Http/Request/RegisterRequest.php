<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'email' => 'required|email',
            'idnum' => 'required',
            'password' => 'required',
            'password_confirm' => 'required'
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'description.required' => 'Debe insertar un nombre a su usuario',
            'email.required' => 'El campo email es obligatorio',
            'email.email' => 'El campo email no es valido',
            'idnum.required' => 'Debe insertar su numero de cedula',
            'password.required' => 'Debe insertar una contrasena',
            'password_confirm.required' => 'Debe confirmar su contrasena'
        ];
    }

}
