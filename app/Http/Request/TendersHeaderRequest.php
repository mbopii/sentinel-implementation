<?php namespace App\Http\Requests;

class TendersHeaderRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tittle'        => 'required',
            'number'        => 'required',
            'awarded_amount'=> 'required|numeric',
            'client_id'     => 'required'
        ];
    }

    public function messages()
    {
        return [
            'tittle.required'           => 'El título de la licitación es requerido',
            'number.required'           => 'El numero de la licitación es requerido',
            'awarded_amount.required'   => 'La cantidad adjudicada es requerida',
            'awarded_amount.numeric'    => 'El valor de la cantidad adjudicada debe ser numerico, sin puntos',
            'client_id.required'        => 'Debe elegir un cliente'
        ];
    }

}
