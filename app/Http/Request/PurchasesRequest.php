<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class PurchasesRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'purchase_date' => 'required|date',
            'invoice_number' => 'required',
            'stamping' => 'required'
        ];
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'purchase_date.required' => 'Debe agregar una fecha de compra',
            'purchase_date.date' => 'El formato de la fecha de compra no es el correcto',
            'invoice_number.required' => 'Debe agregar un numero de factura',
            'stamping.required' => 'Debe agregar el numero de timbrado'
        ];
    }

}
