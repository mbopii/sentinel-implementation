<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

//use Illuminate\Http\Request;

class ClientsRequest extends Request
{
    /**
     * Determine if the user is authorize to make this request
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to this request
     * @return array
     */
    public function rules()
    {
        return [
            'description'   => 'required',
            'address'      => 'required',
            'email'         => 'email'
        ];
    }

    /**
     * Set custom messages for validator errors
     * @return array
     */
    public function messages()
    {
        return [
            'description.required' => 'El campo descripción es obligatorio.',
            'address.required' => 'El campo dirección es obligatorio.',
            'email.email' => 'El formato de correo ingresado es incorrecto'
        ];
    }
}