<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Departments;
use Illuminate\Http\Request;


class DepartmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('departments')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $departments = Departments::paginate(20);

        return view('administration.departments.index', compact('departments'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('departments.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('administration.departments.create');
    }

    public function store(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('departments.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();

        if (is_null($input['description'])){
            \Log::warning("DepartmentsController | Missing description");
            return redirect()->back()->with('error', 'Debe escribir una descripción');
        }

        if (!$brand = Departments::create($input)){
            \Log::warning("DepartmentsController | Ocurrio un error al crear el registro");
            return redirect()->back()->with('error', 'Ocurrio un error al crear el registro');
        }

        return redirect()->route('departments.index')->with('success', 'Registro creado exitosamennte');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('departments.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($department = Departments::find($id)){
            return view('administration.departments.edit', compact('department'));
        }

        \Log::warning("DepartmentsController | Department {$id} not found");

        return redirect()->back()->with('error', 'Departamento no encontrada');
    }

    public function update(Request $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('departments.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($department = Departments::find($id)){
            $input = $request->all();
            if (is_null($input['description'])){
                \Log::warning("DepartmentsController | Missing description");
                return redirect()->back()->with('error', 'Debe escribir una descripción');
            }

            if (!$department->update($input)){
                \Log::warning("DepartmentsController | Error on update");
                return redirect()->back()->with('error', 'Ocurrio un  error al intentar modificar el registro');
            }

            return redirect()->route('departments.index')->with('success', 'Registro modificado correctamente');

        }else{
            \Log::warning("DepartmentsController | Brand {$id} not found");
            return redirect()->back()->with('error', 'Marca no encontrada');
        }

    }
}