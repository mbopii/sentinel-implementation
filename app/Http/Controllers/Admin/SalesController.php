<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Cities;
use App\Models\OrdersDetails;
use App\Models\OrdersHeaders;
use App\Models\PaymentForms;
use App\Models\Products;
use App\Models\User;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('sales')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $input = $request->all();
        if (array_key_exists('q', $input) && $input['q'] != '') {
            $where = '';
            if (!is_null($input['date_start']) && is_null($input['date_end'])) $where .= "created_at::date = '{$input['date_start']}' AND ";
            if (!is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "created_at::date BETWEEN '{$input['date_start']}' AND '{$input['date_end']}' AND ";
            if (is_null($input['date_start']) && !is_null($input['date_end'])) $where .= "created_at::date = '{$input['date_end']}' AND ";
            if (!is_null($input['payment_method_id']) && $input['payment_method_id'] != 0) $where .= "payment_method_id = {$input['payment_method_id']} AND ";

            $where = trim($where, " ");
            $where = trim($where, "AND");
            $where = trim($where, " ");

            if ($where == '') {
                $salesHeader = OrdersHeaders::orderBy('id', 'desc')->paginate(20);
            } else {
                $salesHeader = OrdersHeaders::orderBy('id', 'desc')->whereRaw($where)->paginate(20);
            }
            $date_start = isset($input['date_start']) ? $input['date_start'] : '';
            $date_end = isset($input['date_end']) ? $input['date_end'] : '';
            $payment_method_id = isset($input['payment_method_id']) ? $input['payment_method_id'] : '';
            $description = isset($input['description']) ? $input['description'] : '';
            $q = $input['q'];
        } else {
            $date_start = '';
            $date_end = '';
            $q = '';
            $description = '';
            $payment_method_id = '';
            $salesHeader = OrdersHeaders::orderBy('id', 'desc')->paginate(20);
        }

        $paymentForms = [0 => 'Seleccione..'];
        $paymentForms += PaymentForms::all()->pluck('description', 'id')->toArray();

        return view('administration.sales.index', compact('salesHeader', 'date_start', 'date_end',
            'q', 'description', 'payment_method_id', 'paymentForms'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $productsJson = Products::all();
        $cities = [0 => 'Seleccione..'];
        $cities += Cities::all()->pluck('description', 'id')->toArray();
        return view('administration.sales.create', compact("productsJson", 'cities'));
    }

    public function show($id)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$sale = OrdersHeaders::find($id)) {
            \Log::warning("SalesController | Sale id {$id} not found");
            return redirect()->back()->with('error', 'Registro no encontrado');
        }

        return view("administration.sales.view", compact('sale'));
    }

    public function store(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $input = $request->all();
        $user = User::where('idnum', $input['client_idnum'])->first();

        $orderHeaderArray = [
            'user_id' => $user->id,
            'address_id' => $input['address_id'],
            'status_id' => '1', // En proceso,
            'payment_method_id' => 1,
            'purchase_date' => date('Y-m-d'),
            'order_amount' => 0
        ];

        $amount = 0;
        if ($orderHeader = OrdersHeaders::create($orderHeaderArray)) {
            // Guardamos detalle de factura
            for ($i = 0; $i < count($input['product_id']); $i++) {
                $product = Products::find($input['product_id'][$i]);
                $detailsArray = [
                    'order_header_id' => $orderHeader->id,
                    'product_id' => $product->id,
                    'unit_price' => $product->price,
                    'quantity' => $input['quantity'][$i],
                    'sub_total' => ($input['quantity'][$i] * $product->price)
                ];

                if ($input['discount'][$i] =! 0 && $input['quantity'][$i] > $product->discount_quantity) {
                    $discount = $product->price * $input['quantity'][$i] * $product->discount_percentage / 100;
                    $detailsArray['discount'] = true;
                    $detailsArray['total_discount'] = $discount;
                }
                OrdersDetails::create($detailsArray);
                $amount += $input['quantity'][$i] * $product->price;
                if($product->complex_product){
                    foreach($product->complexItems as $item){
                        $complexItem = Products::find($item->involve_product_id);
                        $complexItem->quantity = $complexItem->quantity - ($item->quantity * $input['quantity'][$i]);
                        $complexItem->save();
                    }
                }else{
                    // Update Stock
                    $product->quantity = $product->quantity - $input['quantity'][$i];
                    $product->save();
                }
            }

            $orderHeader->order_amount = $amount;
            $orderHeader->save();
            return redirect()->route('sales.index')->with('success', 'Venta realizada exitosamente');
        }else{
            \Log::warning("OrdersController | Error while creating a new order");
            return redirect()->back()->with('error', 'Error al crear el registro');
        }
    }

    public function cancelOrder($id)
    {
        if (!\Sentinel::getUser()->hasAccess('sales.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if ($order = OrdersHeaders::find($id)){
            $order->status_id = 2; // canceled
            $order->save();

            // Devolvemos Stock
            foreach ($order->details as $detail){
                $product = Products::find($detail->product_id);
                if ($product->complex_product) {
                    foreach ($product->complexItems as $item) {
                        $complexItem = Products::find($item->involve_product_id);
                        $complexItem->quantity = $complexItem->quantity + ($item->quantity * $detail->qty);
                        $complexItem->save();
                    }
                } else {
                    // Update Stock
                    $product->quantity = $product->quantity + $detail->quantity;
                    $product->save();
                }
            }

            return redirect()->route("sales.index")->with('success', 'Orden Cancelada Exitosamente');
        }else{
            \Log::warning("OrdersController | Order {$id} not foudn");
            return redirect()->back()->with('error', 'Orden no encontrada');
        }

    }
}
