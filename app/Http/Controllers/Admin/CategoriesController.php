<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Sentinel::getUser()->hasAccess('categories')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }
        $categories = Categories::paginate(20);

        return view('administration.categories.index', compact('categories'));
    }

    public function create()
    {
        if (!\Sentinel::getUser()->hasAccess('categories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        return view('administration.categories.create');
    }

    public function store(Request $request)
    {
        if (!\Sentinel::getUser()->hasAccess('categories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        $description = $request->get('description');
        if (empty($description)){
            \Log::warning("CategoriesController | Missing description");
            return redirect()->back()->with('error', 'Debe introducir una descripcion');
        }

        $arrayToSave = [
            'description' => $description
        ];

        if (!$category = Categories::create($arrayToSave)){
            \Log::warning("CategoriesContrller | Error while trying to save new category");
            return redirect()->back()->with('error', 'Ocurrio un error al intentar guardar el registro');
        }
        $input = $request->all();

        if (array_key_exists('category_image', $input) && !is_null($input['category_image'])){
            $fileName =  $category->id . '.jpg';
            $input['category_image']->move(public_path('admin/images/categories/'), $fileName);
        }

        return redirect()->route('categories.index')->with('success', 'Categoria creada exitosamente');
    }

    public function edit($id)
    {
        if (!\Sentinel::getUser()->hasAccess('categories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$category = Categories::find($id)){
            \Log::warning("CategoriesController | Category {$id} not found");
            return redirect()->back()->with('error', 'Categoria no encontrada');
        }

        return view('administration.categories.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        if (!\Sentinel::getUser()->hasAccess('categories.add|edit')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            return redirect('/')->with('error', 'No posee permisos para realizar esta accion.');
        }

        if (!$category = Categories::find($id)){
            \Log::warning("CategoriesController | Category {$id} not found");
            return redirect()->back()->with('error', 'Categoria no encontrada');
        }

        $description = $request->get('description');
        if (empty($description)){
            \Log::warning("CategoriesController | Missing description");
            return redirect()->back()->with('error', 'Debe introducir una descripcion');
        }

        $category->description = $description;

        if (!$category->save()){
            \Log::warning("CategoriesController | Error on update category id {$id}");
            return redirect()->back()->with('error', 'Error al intentar actualizar el registro');
        }
        $input = $request->all();
        if (array_key_exists('category_image', $input) && !is_null($input['category_image'])){
            $fileName =  $category->id . '.jpg';
            $input['category_image']->move(public_path('admin/images/categories/'), $fileName);
        }

        return redirect()->route('categories.index')->with('success', 'Registro actualizado exitosamente');
    }

    public function destroy($id)
    {
        $message = '';
        $error = '';

        if (!\Sentinel::getUser()->hasAccess('categories.destroy')) {
            \Log::error('Unauthorized access attempt',
                ['user' => \Sentinel::getUser()->username, 'action' => \Request::route()->getActionName()]);
            $message = 'No posee permisos para realizar esta operacion';
            $error = true;
        } else {
            if ($role = Categories::find($id)) {
                try {
                    if (Categories::destroy($id)) {
                        $message = 'Categoria eliminada correctamente';
                        $error = false;
                    }
                } catch (\Exception $e) {
                    \Log::warning("CategoriesController | Error deleting Category: " . $e->getMessage());
                    $message = 'Error al intentar eliminar la categoria';
                    $error = true;
                }
            } else {
                \Log::warning("CategoriesController | Category {$id} not found");
                $message = 'Categoria no encontrada';
                $error = true;
            }
        }


        return response()->json([
            'error' => $error,
            'message' => $message,
        ]);
    }

}