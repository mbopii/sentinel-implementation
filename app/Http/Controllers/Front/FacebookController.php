<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Models\UserTaxInfo;
use Laravel\Socialite\Facades\Socialite;
use Exception;

class FacebookController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }


    /**
     * Create a new controller instance.
     *
     */
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
            \Log::info("FacebookController | New user: " . json_encode($user));
            $create['name'] = $user->getName();
            $create['email'] = $user->getEmail();
            $create['facebook_id'] = $user->getId();


            if ($findUser = User::where('email', $create['email'])->first()) {
                \Log::info("FacebookController | User already exist in the system");
                $findUser->facebook_id = $create['facebook_id'];
                $findUser->save();
                \Sentinel::login(\Sentinel::findById($findUser->id));
                return redirect()->route('index')->with('success', 'Sesion iniciada correctamente');
            } else {
                // Creamos un nuevo usuario
                $credentials = [
                    'email' => $create['email'],
                    'password' => '123456',
                    'description' => $create['name'],
                    'facebook_id' => $create['facebook_id'],
                    'idnum' => '123',
                    'tax_name' => 'Sin Nombre',
                    'tax_code' => '4444444-4',
                ];

                if ($user = \Sentinel::registerAndActivate($credentials)) {
                    // Attach client Role
                    $expectedRole = Role::where('slug', '=', 'client')->first();
//                $expectedRole = Role::where('slug', '=', 'superuser')->first();
                    $user->roles()->attach($expectedRole->id);

                    $user->permissions = $expectedRole->permissions;

                    if (!$user->save()) {
                        \Log::warning('RegisterController | Cant attach permissions', json_encode($credentials));
                        return redirect()
                            ->back()
                            ->withInput()
                            ->with('error', 'Problemas al actualizar registro.');
                    }

                    \Sentinel::login(\Sentinel::findById($user->id));
                    return redirect()->route('front.user.show', $user->id)->with('success', 'Usuario creado exitosamente');
                }
            }


        } catch (Exception $e) {

            \Log::warning("FacebookController | {$e->getMessage()}");
            return redirect()->route('index');


        }
    }
}