<?php

namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Products;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'aboutUs', 'showContact', 'changeLanguage']]);
    }

    public function index()
    {
        $categories = Categories::all();

        $newProducts = Products::orderBy('id', 'desc')->limit(8)->get(); // TODO change for most popular
        $listCategories = Categories::orderBy('id', 'asc')->limit(4)->get();
        $featuredProducts = Products::where('featured', true)->limit(4)->orderBy('id', 'desc')->get();

        // Words Section

        return view('front.welcome', compact('categories', 'navBarCategories', 'newProducts', 'listCategories', 'featuredProducts'));
    }

    public function aboutUs()
    {
        $categories = Categories::all();
        return view('front.various.about', compact('categories'));
    }

    public function showContact()
    {
        $categories = Categories::all();
        return view('front.various.contact', compact('categories'));
    }

    public function changeLanguage($lang)
    {
        \Session::put('applocale', $lang);
        return redirect()->back();
    }

}