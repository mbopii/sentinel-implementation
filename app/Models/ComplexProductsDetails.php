<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComplexProductsDetails extends Model
{
    protected $table = 'complex_products_details';

    protected $fillable = ['product_id', 'quantity', 'involve_product_id'];


    public function productFather()
    {
        return $this->hasMany('App\Models\Products', 'product_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products', 'involve_product_id');
    }
}