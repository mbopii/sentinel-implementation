<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model
{
    protected $table = 'sub_categories';

    protected $fillable = ['description', 'category_id'];

    public function category()
    {
        return $this->belongsTo('App\Models\Categories', 'category_id');
    }
}
