<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';
    protected $fillable = ['description'];

    public function subcategories()
    {
        return $this->hasMany('App\Models\SubCategories', 'category_id');
    }

    public function Products()
    {
        return $this->hasMany('App\Models\Products');
    }

    public function productsCount()
    {
        return Products::where('category_id', $this->id)->count('*');
    }


}
