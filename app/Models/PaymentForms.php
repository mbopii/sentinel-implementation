<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentForms extends Model
{
    protected $table = 'payments_method';

    protected $fillable = ['description'];

}
