<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    protected $table = 'wish_list';

    protected $fillable = ['user_id', 'product_id', 'wish_list_date'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function products()
    {
        return $this->belongsTo('App\Models\Products', 'product_id');
    }
}
